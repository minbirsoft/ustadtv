-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 11, 2013 at 12:21 AM
-- Server version: 5.5.28
-- PHP Version: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ustadtv`
--

-- --------------------------------------------------------

--
-- Table structure for table `adversitements`
--

CREATE TABLE IF NOT EXISTS `adversitements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `adversitements`
--

INSERT INTO `adversitements` (`id`, `title`, `note`, `video`) VALUES
(1, 'Reklam 3', '', 'https://www.youtube.com/watch?v=z5abFrz_4HM');

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE IF NOT EXISTS `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`id`, `parent_id`, `slug`, `name`, `tags`) VALUES
(1, NULL, 'psixologiya', 'Psixologiya', 'psixoloji xəstəliklər'),
(5, NULL, 'saglamliq', 'Sağlamlıq', 'sağlamlıq, idman, inkişaf, qidalanma, sağlam həyat'),
(17, NULL, 'gözəllik', 'Gözəllik', 'gözəllik, üz cizgiləri, dərinin həssaslığı, həyatınızın dəriyə təsiri, stress gözəlliyə qarşıdırmı'),
(18, NULL, 'astrologiya', 'Astrologiya', 'səma cisimləri, kosmos, boşluq, fəza, ulduzlar, planetlər, bürclər'),
(19, NULL, 'hugug', 'Hüquq', 'haqqlarınız, insanlar, milli məclis'),
(20, NULL, 'biznes', 'Biznes', 'biznes, isler, haqqimizda'),
(21, 20, 'marketing', 'Marketinq', 'marketing'),
(22, 20, 'hr', 'HR', 'human resources'),
(23, NULL, 'din', 'Din', 'islam, xristianliq, yehudi'),
(24, NULL, 'musiqi', 'Musiqi', 'mahni, mugam, hiphop, pop'),
(25, 24, 'guitar', 'Gitara', 'gitara, notlar'),
(26, NULL, 'gundelikheyat', 'Gündəlik Həyat', 'yemek'),
(27, 26, 'qidamehsullari', 'Qida Məhsulları', 'insanlar'),
(28, NULL, 'idman', 'İdman', 'Yeni kanal'),
(29, 5, 'xesteklikler', 'Xəstəliklər', '');

-- --------------------------------------------------------

--
-- Table structure for table `channels_users`
--

CREATE TABLE IF NOT EXISTS `channels_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `channel_id` (`channel_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `channels_users`
--

INSERT INTO `channels_users` (`id`, `channel_id`, `user_id`) VALUES
(1, 1, 1),
(2, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `episodes`
--

CREATE TABLE IF NOT EXISTS `episodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slider_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adversitement_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `viewcount` int(11) NOT NULL,
  `likecount` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `program_id` (`program_id`),
  KEY `adversitement_id` (`adversitement_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `episodes`
--

INSERT INTO `episodes` (`id`, `program_id`, `title`, `slug`, `description`, `tags`, `video`, `photo`, `slider_photo`, `adversitement_id`, `created`, `viewcount`, `likecount`) VALUES
(4, 7, 'Panik atak nöbeti', 'Panikataknobeti', 'Panik atak nöbeti geçirdiğimizi anlarsak ne yapmalıyız?', 'panik\r\n', 'http://www.youtube.com/watch?v=cdgQpa1pUUE', '7226ddad6f926d1c460c215434b804f3.jpg', 'c8e0fe49aff093070c19d983c8226511.jpg', NULL, '2012-12-19 22:15:43', 46, 32),
(5, 7, 'Yabancı ülkelerde marjinal', 'yabanciulkelerdemarjinal', 'Yabancı ülkelerde marjinal gece hayatı ile ünlü şehirler hangileridir?\r\n', 'yurtdışı geceleri, gece kulübü, yurtdışında gece hayatı, yurtdışı gezisi, marjinal gece hayatı  hepsi\r\n', 'http://www.youtube.com/watch?v=eQf-NGYVQ4E', '6bb23a9f7cb7dbd35141e01661015356.jpg', '8d8894dae3b7bcebccefde0eb59f4d49.jpg', NULL, '2012-12-20 09:16:21', 35, 1),
(6, 8, 'Yurtdışında yüksek lisans için ne tür burs imkanları var?', 'yurtdisinda_yuksek_lisans', 'Yurtdışında yüksek lisans yapmadan önce bilmeniz gerekenler... Lisans eğitiminden sonra yurtdışında yüksek lisans eğitimi almak isteyen öğrenciler nasıl bir yol takip etmeli? Yurtdışında hangi ülkelerde yüksek lisans yapabiliriz? Yabancı üniversitele', 'yüksek lisans, yurtdışı master, abd yüksek lisans, abd yüksek lisans, ingilterede yüksek lisans, yurtdışında master yapmak, yurtdışında yüksek lisans, yüksek lisans yurtdışı, yurtdışında yüksek lisans', 'https://www.youtube.com/watch?v=SxO-6SaNMvg', '93250477633d65d7fea1a538badf8d80.jpg', 'fa30a2d68bf85d89078094d4ece5999c.jpg', NULL, '2012-12-20 09:20:15', 195, 16),
(7, 9, 'Qrip necə yayılır', 'qrip-nece-yayilir', 'Qripin necə yayıldığını öyrənin', 'qrip, yayılmaq, xəstəlik', 'https://www.youtube.com/watch?v=Lf0LJpzDTtQ', 'a7da01050867c7a06102b2569c3cb7b7.jpg', '13932dcf91388920c12cb6ec3ccc8088.jpg', NULL, '2012-12-26 08:46:25', 27, 0);

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE IF NOT EXISTS `programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `channel_id` int(11) NOT NULL,
  `ustad_id` int(11) NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ustad_id` (`ustad_id`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `slug`, `title`, `channel_id`, `ustad_id`, `description`, `photo`, `tags`, `created`) VALUES
(7, 'panikatak', 'Panik Atak Nöbetleri', 1, 8, 'Panik atak nöbetleri hakkında bilmeniz gerekenler... Panik atak nöbetleri ne sıklıkta olur? Ne zaman ortaya çıkacağı önceden bilinebilir mi? Kişinin panik atak nöbeti geçirmesine neden olan faktörler var mıdır yoksa atak nedensiz ve her zaman ortaya ', 'ea82dbc24b59843a88236de8eba64767.jpg', 'panika, atak, urek', '2012-12-19 22:09:36'),
(8, 'yilbasi_menusu_hazirlama', 'Yılbaşı Menüsü Hazırlama ', 27, 11, 'Yılbaşı menüsü hazırlarken dikkat etmeniz gerekenler... Yılbaşı menüsü nasıl planlanır? Menüyü belirlerken dikkat etmemiz gereken belli başlı noktalar nelerdir? Yılbaşı yemeği için en uygun ana yemekler hangileridir? Hindinin yanına başka ne yapabili', '27276a5d55bcd9bca4532ca37228a35d.jpg', 'yemek, davet, yılbaşı yemeği düzenleme, yılbaşı yemeği düzenleme, yılbaşı için soğuk meze, yılbaşı için ara sıcak, yılbaşı daveti, hindi pişirme, yılbaşında yemek, yılbaşında yemek, davetli, yılbaşı y', '2012-12-20 09:18:14'),
(9, 'qrip', 'Qrip', 29, 5, '', 'db652502cf36664f4909f113bb55e105.jpg', 'xestelik, xəstəlik, qrip', '2012-12-26 08:44:46');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_uid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `registered` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `fullname`, `facebook_uid`, `registered`) VALUES
(1, 'demo', 'demo@demo.com', '07c45b82dc7216c07ef94025a715c6526ba8f87e', 'Demo Demozade', '123456', '2012-12-03 00:00:00'),
(2, 'test', 'test@test.com', 'c63a14b1c1c45b74ae4148139b87754390b2bb6b', 'Test, Sinaq', '', '0000-00-00 00:00:00'),
(3, 'demo2', 'demo2@demo.com', '190e7d6fd4e564472ce5bacaa2184e39c2b5e754', 'demo2', '', '0000-00-00 00:00:00'),
(4, 'jtogrul', 'ceferlitogrul@gmail.com', '', 'Togrul Jafar', '', '0000-00-00 00:00:00'),
(5, 'demo5', 'demo5@demo.com', '190e7d6fd4e564472ce5bacaa2184e39c2b5e754', 'Demo 5 Demo5', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_like_episodes`
--

CREATE TABLE IF NOT EXISTS `users_like_episodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `episode_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`episode_id`),
  KEY `user_id` (`user_id`,`episode_id`),
  KEY `episode_id` (`episode_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `users_like_episodes`
--

INSERT INTO `users_like_episodes` (`id`, `user_id`, `episode_id`) VALUES
(38, 3, 4),
(37, 3, 5),
(36, 3, 6);

-- --------------------------------------------------------

--
-- Table structure for table `ustads`
--

CREATE TABLE IF NOT EXISTS `ustads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `profession` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `info` text COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ustads`
--

INSERT INTO `ustads` (`id`, `fullname`, `profession`, `info`, `photo`) VALUES
(5, 'Məhərrəmov Rasim', 'Kardioloq', 'Kardioloq09.07.1981 tarixində Cəbrayil rayonunda doğulmuşdur. 1998-ci ildə Azərbaycan Dövlət Tibb Univetsitetinin Pediatriya fakultəsinə daxil olmuşdur. 2004-cü ildə Topçubaşov adına ETCİ-də kardiocərrahiyyə ixtisası üzrə, 2005-ci ilin sonunda ABŞ-ın Houston şəhərində ixtisas artırma kursu keçmişdir. 2006-2009-cu illərdə Turkiyənin paytaxtı Ankara şəhərində kardioloji ixtisas almış, eləcə də 2010-2011-ci illərdə Almaniya və Türkiyədə kurslara qatılmışdır. Hal-hazırda özəl tibb müəsisəsində çalışır. Ailəlidir: 2 övladı var.', 'bbadb37fcc685af0b821fe4b8bf54dce.jpg'),
(6, 'Kərimova Səriyyə', 'Həkim-trixoloq', '1961-ci ildə doğulmuşdur. 1983-1990-cı illərdə ADTİ Pediatriya fakultəsində təhsil almışdır. 1991-ci ildə ADTİ-də işləməyə başlamışdır. ADTİ Tüklərin Tədqiqinə dair elmi problemlər laboratoriyasının kiçik elmi işçisidir. Eyni zamanda Hi-Fi estetik mərkəzində həkim-trixoloq kimi fəaliyyət göstərir. Ailəlidi, 2 övladı var.', '48c7d5ea9d6ad17d22a9faeee273a54a.jpg'),
(7, 'Firudin Qurbansoy', 'Astroloq', '1954-cü ildə Bakı şəhərində anadan olmuşdur. 1977-ci ildə Azərbaycan Dövlət Pedaqoji Universitetinin Filalogiya fakültəsinə daxil olmuşdur. 1991-ci ildə “Məhəmməd Füzuli şeir sənətində bədii obraz ifadə vasitələri” mövzusunu müdafiə edərək Filologiya elmlər namizədi adını qazanmışdır. 1989-cu ildə Azərbaycan radiosunda həftəlik münəccim aparmışdır. Azərbaycanda qurulan Aloinform sual-cavab xəttini aparmışdır. 1998-ci ildən müəllifi olduğu hər bürc üçün gündəlik bürclər kitabı çıxır. 100-dən çox elmi məqalənin müəllifidir. Halhazırda “Orta əsrlər Azərbaycan ədəbiyyatı ənənəsində irfan (sufi) rəmzlərinin tarixi təkamülü“ adlı doktorluq disertasiyasını müdafiə edir.', '3ab48b7855c4914123c223359cd6a406.jpg'),
(8, 'Sevinc Məhərrəmova', 'Psixoloq', '1983-cü il may ayının 21-də Cəbrayıl şəhərində dünyaya gəlmişdir. 10-cu sinifdə oxuyarkən kimya-biologiya təmayüllü Respublika Litseyinə qəbul olmuş və elə həmin litseydən məzun olmuşdur. 2000-ci ildə Azərbaycan Dövlət Tibb Universitenin Müalicə işi fakültəsinə daxil olmuş, 2006-cı ildə universiteti bitirmişdir. Ümumi terapiya üzrə mütəxəssisləşdikdən sonra Psixologiya üzrə kurs keçmək üçün Türkiyənin İzmir şəhərində Ege Universitetinin psixoloqu Ufuk Solakdan dərslər almışdır. Azərbaycanda Araz Manuçehrlinin rəhbərliyi ilə psixoterapiya üzrə mütəxəssisləşmiş və müxtəlif tibb jurnallarında məqalələr dərc etmişdir. 2009-cu ildən 20 saylı poliklinikada psixoterapevt kimi fəaliyyət göstərir. Ailəlidir, 1qızı var.\r\n', '66b5d2021e86834a4eacd38de3ca1842.jpg'),
(9, 'Rəşid Mahmudov', 'Fizioloq', '1938-ci ildə Gədəbəy rayonunun Kəsəmənli kəndində anadan olmuşdur. Böyük Qaramat kənd orta məktəbində təhsil aldıqdan sonra təhsilini 6 il Azərbaycan Tibb Universitetində davam etdirmişdir. Sonralar Gədəbəyin Şınıx kəndində “Rəsullu” kənd xəstəxanasının müdir vəzifəsində işləmişdir. 3 il Azərbaycan Tibb Universitetində fiziologiya üzrə aspirantura təhsili alıb. Hal hazırda dossent, əməkdar müəllim kimi fəaliyyət göstərir. Ailəlidir, 3 övladı var. ', '2cbaa9c948ecc61365a97f75da28d548.jpg'),
(10, 'Vüsal Qəmbərov', 'Marketing', '“ 9 Eylül Universiteti ”- də Marketing təhsili alıb, “Marmara Universiteti”- nin Doktorantıdır. Hal-hazırda bir çox şirkətdə Biznes Məsləhətçisi olaraq çalışır. Eləcə də, Azərbaycan Universitetində Marketing fənnini tədris edir. ', '9267b4da0c35dfb3f457a6874cd36193.jpg'),
(11, 'Leyla Əmirova ', 'HR Mütəxəssis', '“Azərbaycan Dillər Universiteti”- nin İngilis dili fakultəsini bitirmişdir, Ölkə xaricində İqtisadivə Şəxsi İnkişaf yönümlü təlimlərdə iştirak etmişdir. “Uğurlu müsahibə” , “Yeni işə Adaptasiya” , “Biznes Englihs” üzrə təlimçidir. Hal-hazırda “Komtec LTD” şirkətində HR mütəxəssis olaraq çalışır.', 'f2e6e19ad8f31f12ccccd976a17f7ad1.jpg');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `channels`
--
ALTER TABLE `channels`
  ADD CONSTRAINT `channels_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `channels` (`id`);

--
-- Constraints for table `channels_users`
--
ALTER TABLE `channels_users`
  ADD CONSTRAINT `channels_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `channels_users_ibfk_2` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`);

--
-- Constraints for table `episodes`
--
ALTER TABLE `episodes`
  ADD CONSTRAINT `episodes_ibfk_2` FOREIGN KEY (`adversitement_id`) REFERENCES `adversitements` (`id`),
  ADD CONSTRAINT `episodes_ibfk_1` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `programs`
--
ALTER TABLE `programs`
  ADD CONSTRAINT `programs_ibfk_1` FOREIGN KEY (`ustad_id`) REFERENCES `ustads` (`id`),
  ADD CONSTRAINT `programs_ibfk_2` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`);

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users_like_episodes`
--
ALTER TABLE `users_like_episodes`
  ADD CONSTRAINT `users_like_episodes_ibfk_1` FOREIGN KEY (`episode_id`) REFERENCES `episodes` (`id`),
  ADD CONSTRAINT `users_like_episodes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
