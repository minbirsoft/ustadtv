<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    
    <?php foreach ($episodes as $episode): ?>
    <url>
        <loc><?php echo Router::url(array('controller'=>'main','action'=>'episode','id'=>$episode['Episode']['id'],'slug'=>$episode['Episode']['slug'])); ?></loc>
        <lastmod><?php echo $this->Time->toAtom($episode['Episode']['created']); ?></lastmod>
        <changefreq>weekly</changefreq>
    </url>
    <?php endforeach; ?>
    
    <?php foreach ($programs as $program): ?>
    <url>
        <loc><?php echo Router::url(array('controller'=>'main','action'=>'program','id'=>$program['Program']['id'],'slug'=>$program['Program']['slug'])); ?></loc>
        <lastmod><?php echo $this->Time->toAtom($program['Program']['created']); ?></lastmod>
        <changefreq>weekly</changefreq>
    </url>
    <?php endforeach; ?>
    
    <?php foreach ($ustads as $ustad): ?>
    <url>
        <loc><?php echo Router::url(array('controller'=>'main','action'=>'ustad','id'=>$ustad['Ustad']['id'])); ?></loc>
        <changefreq>weekly</changefreq>
    </url>
    <?php endforeach; ?>
    
    <?php foreach ($channels as $channel): ?>
    <url>
        <loc><?php echo Router::url(array('controller'=>'main','action'=>'channel','id'=>$channel['Channel']['id'],'slug'=>$channel['Channel']['slug'])); ?></loc>
        <changefreq>weekly</changefreq>
    </url>
    <?php endforeach; ?>
    
</urlset>