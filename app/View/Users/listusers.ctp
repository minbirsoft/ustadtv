<?php
$this->layout = 'admin';
?>
<h1>İstifadəçilərin siyahısı</h1>
<p><?php echo $this->Html->link("Yeni Istifadəçi", array('action' => 'adduser')); ?></p>
<table>
    <tr>
        <th>Nömrə</th>
        <th>Ad, Soyad</th>
        <th>İstifadəçi adı</th>
        <th>Email</th>
        <th>Əməliyyat</th>
    </tr>

    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['id']; ?></td>
        <td><?php echo $user['User']['fullname']; ?></td>
        <td><?php echo $user['User']['username']; ?></td>
        <td><?php echo $user['User']['email']; ?></td>
        <td><?php echo $this->Html->link('Düzəliş et', array('action' => 'edituser', $user['User']['id'])); ?>
            <?php echo $this->Form->postLink( 'Sil',
                          array('action' => 'deleteuser', $user['User']['id']),
                          array('confirm' => 'Silmək istədiyinizə əminsiniz?')
                       ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($users); ?>
</table>
<?php
echo $this->Paginator->numbers(array(
    'before' => 'Səhifələmək: ',
    'first' => 2, 
    'last' => 2
    ));
?>