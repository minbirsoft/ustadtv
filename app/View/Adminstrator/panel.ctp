<?php
$this->layout = 'admin';
?>
<h1>Adminstrator Paneli</h1>
<br/><br/><br/>
<div class="message success">
    <?php echo $this->Html->link('Kanallar', array('controller' => 'channels', 'action' => 'listchannels'));  ?> <br/>
    Kanalların və iyerarxiyanın idarəedilməsi, yeni kanalların əlavə edilməsi və s.
</div>
<div class="message success">
    <?php echo $this->Html->link('Proqramlar', array('controller' => 'programs', 'action' => 'listprograms'));  ?> <br/>
    Proqramların idarədilməsi, yeni proqramların yaradılması və s.
</div>
<div class="message success">
    <?php echo $this->Html->link('Cavablar', array('controller' => 'episodes', 'action' => 'listepisodes'));  ?> <br/>
    Cavabların (Videoların) idarəedilməsi, yeni cavabların yüklənilməsi və s.
</div>
<div class="message success">
    <?php echo $this->Html->link('Ustadlar', array('controller' => 'ustads', 'action' => 'listustads'));  ?> <br/>
    Ustadların siyahısının idarə olunması, məlumatlara düzəliş və s.
</div>
<div class="message success">
    <?php echo $this->Html->link('Istifadəçilər', array('controller' => 'users', 'action' => 'listusers'));  ?> <br/>
    İstifadəçilərin siyahısı. Məlumatların idarə olunması, məlumatlara düzəliş və s.
</div>
<div class="message success">
    <?php echo $this->Html->link('Video Reklamlar', array('controller' => 'adversitements', 'action' => 'listadversitements'));  ?> <br/>
    Videoöncəsi reklamların idarə olunması, reklamların hansı kanallarda göstərilməsinə nəzarət.
</div>
<div class="message success">
    <?php echo $this->Html->link('Suallar', array('controller' => 'questions', 'action' => 'listquestions'));  ?> <br/>
    Istifadəçilər tərəfindən göndərilmiş suallar
</div>