<?php
$this->layout = 'admin';
?>

<h1>Reklamların siyahısı</h1>
<p><?php echo $this->Html->link("Yeni Reklam", array('action' => 'addadversitement')); ?></p>
<table>
    <tr>
        <th>Nömrə</th>
        <th>Başlıq</th>
        <th>Əməliyyat</th>
    </tr>

    <?php foreach ($adversitements as $adversitement): ?>
    <tr>
        <td><?php echo $adversitement['Adversitement']['id']; ?></td>
        <td><?php echo $adversitement['Adversitement']['title']; ?></td>
        <td><?php echo $this->Html->link('Düzəliş et', array('action' => 'editadversitement', $adversitement['Adversitement']['id'])); ?>
            <?php echo $this->Form->postLink( 'Sil',
                          array('action' => 'deleteadversitement', $adversitement['Adversitement']['id']),
                          array('confirm' => 'Silmək istədiyinizə əminsiniz?')
                       ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($adversitements); ?>
</table>