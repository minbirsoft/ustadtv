<?php
$cakeDescription = __d('ustadtv', 'UstadTv');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css('cake.generic');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
    </head>
    <body>
        <div id="container">

            <div id="header">
                <h1>
                    <?php echo $this->Html->link('Kanallar', array('controller' => 'channels', 'action' => 'listchannels')); ?> · 
                    <?php echo $this->Html->link('Proqramlar', array('controller' => 'programs', 'action' => 'listprograms')); ?> · 
                    <?php echo $this->Html->link('Cavablar', array('controller' => 'episodes', 'action' => 'listepisodes')); ?> · 
                    <?php echo $this->Html->link('Ustadlar', array('controller' => 'ustads', 'action' => 'listustads')); ?> · 
                    <?php echo $this->Html->link('Istifadəçilər', array('controller' => 'users', 'action' => 'listusers')); ?> · 
                    <?php echo $this->Html->link('Video Reklamlar', array('controller' => 'adversitements', 'action' => 'listadversitements')); ?> · 
                    <?php echo $this->Html->link('Suallar', array('controller' => 'questions', 'action' => 'listquestions')); ?>
                    <div style="float:right;"><?php echo $this->Html->link('Çıxış', array('controller' => 'adminstrator', 'action' => 'logout')); ?></div>
                </h1>
            </div>
            <div id="content">

                <?php echo $this->Session->flash(); ?>

                <?php echo $this->fetch('content'); ?>
            </div>


            <div id="footer">
                <?php
                echo $this->Html->link(
                        $this->Html->image('MinBirSoft', array('alt' => $cakeDescription, 'border' => '0')), 'http://minbirsoft.com', array('target' => '_blank', 'escape' => false)
                );
                ?>
            </div>
        </div>
        <!--
        <?php //echo $this->element('sql_dump');  ?> -->
    </body>
</html>
