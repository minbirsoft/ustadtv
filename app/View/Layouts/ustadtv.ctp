<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $title_for_layout; ?> — UstadTv.Az</title>
        <?php
        echo $this->Html->meta('icon');

        echo $this->Html->meta('rss', '/feed/allepisodes.rss', array('type' => 'rss'));

        if (!isset($meta_keywords)) {
            $meta_keywords = 'ustadtv, ustad, meslehet, məsləhət, sual, cavab, video, onlayn, öyrən';
        }
        echo $this->Html->meta('keywords', $meta_keywords);

        if (!isset($meta_description)) {
            $meta_description = $title_for_layout;
        }
        echo $this->Html->meta('description', $meta_description);
        
        echo $this->Html->meta(array('property' => 'fb:app_id', 'content' => Configure::read("FB_APP_ID")));


        echo $this->Html->css('style');
        echo $this->Html->script('jquery-1.8.3.min');
        echo $this->Html->script('jquery.noty');
        echo $this->Html->script('noty.theme.default');
        echo $this->Html->script('noty.topCenter');
        echo $this->Html->script('noty.center');
        echo $this->Html->script('script');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <meta charset="utf-8"/>
    </head>
    <body>

        <?php echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>
        
        <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/az_AZ/all.js#xfbml=1&appId=<?php echo Configure::read("FB_APP_ID");?>";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

    </body>
</html>
