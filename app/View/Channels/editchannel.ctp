<?php
$this->layout = 'admin';
?>
<h1>Kanala düzəliş et</h1>
<?php
echo $this->Form->create('Post');
echo $this->Form->input('Channel.name');
echo $this->Form->input('Channel.slug');
echo $this->Form->input('Channel.parent_id', array('type'=>'select','options'=>$channels, 'empty' => '-- Valideynsiz'));
echo $this->Form->input('Channel.tags', array('rows' => '2'));
echo $this->Form->end('Düzəlişi yadda saxla');
?>