<?php
$this->layout = 'admin';
?>
<h1>Kanalların siyahısı</h1>
<p><?php echo $this->Html->link("Yeni Kanal", array('action' => 'addchannel')); ?></p>
<table>
    <tr>
        <th>Nömrə</th>
        <th>Valideyn</th>
        <th>Başlıq</th>
        <th>Əməliyyat</th>
    </tr>

    <?php foreach ($channels as $channel): ?>
    <tr>
        <td><?php echo $channel['Channel']['id']; ?></td>
        <td><?php if( isset($channel['Channel']['parent_id']) ) {
                echo $channel['Channel']['parent_id'];
            } else {
                echo $this->Html->link('Alt Kanalları', array(
                    '?' => array('listby'=>'channel', 'withid'=>$channel['Channel']['id'])
                    ));
            } ?>
        </td>
        <td><?php echo $channel['Channel']['name']; ?></td>
        <td><?php echo $this->Html->link('Düzəliş et', array('action' => 'editchannel', $channel['Channel']['id'])); ?>
            <?php echo $this->Form->postLink( 'Sil',
                          array('action' => 'deletechannel', $channel['Channel']['id']),
                          array('confirm' => 'Silmək istədiyinizə əminsiniz?')
                       ); ?>
            <?php echo $this->Html->link('Proqramlar', array(
                'controller' => 'programs',
                'action' => 'listprograms',
                '?' => array('listby'=>'channel', 'withid'=>$channel['Channel']['id'])
                )); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($post); ?>
</table>