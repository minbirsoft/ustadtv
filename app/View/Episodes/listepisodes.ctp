<?php
$this->layout = 'admin';
?>
<h1>Epizodların siyahısı</h1>
<p><?php echo $this->Html->link("Yeni Epizod", array('action' => 'addepisode')); ?></p>
<table>
    <tr>
        <th>Nömrə</th>
        <th>Proqram</th>
        <th>Başlıq</th>
        <th>Əməliyyat</th>
    </tr>

    <?php foreach ($episodes as $episode): ?>
    <tr>
        <td><?php echo $episode['Episode']['id']; ?></td>
        <td><?php echo $episode['Episode']['program_id']; ?></td>
        <td><?php echo $episode['Episode']['title']; ?></td>
        <td><?php echo $this->Html->link('Düzəliş et', array('action' => 'editepisode', $episode['Episode']['id'])); ?>
            <?php echo $this->Form->postLink( 'Sil',
                          array('action' => 'deleteepisode', $episode['Episode']['id']),
                          array('confirm' => 'Silmək istədiyinizə əminsiniz?')
                       ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($episodes); ?>
</table>
<?php
echo $this->Paginator->numbers(array(
    'before' => 'Səhifələmək: ',
    'first' => 2, 
    'last' => 2
    ));
?>