<?php
$this->layout = 'admin';
echo $this->Html->script('jquery-1.8.3.min');
echo $this->Html->script('jquery.uploadify.min');
echo $this->Html->script('uploadifyscript');
echo $this->Html->css('uploadify');
?>


<?php $timestamp = time(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        var timestamp = '<?php echo $timestamp; ?>';
        var token = '<?php echo md5('ustadtv_salt' . $timestamp); ?>';
        //uploadifyIt('video_upload', 'EpisodeVideo', 'Video seç', 'videos', timestamp, token , '*.mp4', 'video');
        uploadifyIt('photo_upload', 'EpisodePhoto', 'Şəkil seç', 'photos', timestamp, token , '*.jpg;*.png', 'episode');
        uploadifyIt('slide_photo_upload', 'EpisodeSliderPhoto', 'Şəkil seç', 'photos', timestamp, token , '*.jpg;*.png', 'slider');
    });
</script>

<h1>Cavab əlavə et</h1>
<?php
echo $this->Form->create('Post', array('type' => 'file'));
echo $this->Form->input('Episode.title');
echo $this->Form->input('Episode.slug');
echo $this->Form->input('Episode.description', array('rows' => '2'));
echo $this->Form->input('Episode.program_id', array('type' => 'select', 'options' => $programs));
echo $this->Form->input('Episode.tags', array('rows' => '2'));
echo $this->Form->input('Episode.video');
echo $this->Form->input('Episode.photo');
echo $this->Form->input('Episode.slider_photo');
echo $this->Form->input('Episode.adversitement_id', array('type'=>'select','options'=>$adversitements, 'empty' => '-- Reklamsız'));
?>
<!--
Videonun yüklənilməsi:
<input type="file" name="video_upload" id="video_upload" /> -->

Şəklin yüklənilməsi: 
<input type="file" name="photo_upload" id="photo_upload" />

Slayder şəklinin yüklənilməsi: 
<input type="file" name="slide_photo_upload" id="slide_photo_upload" />

<?php
echo $this->Form->end('Əlavə et');
?>
