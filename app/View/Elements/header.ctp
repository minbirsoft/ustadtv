<div style="width:100%; height: 58px; background-color: #fff;border-bottom: 2px solid #D1E751;">
<div id="header">
    <div id="logo">
        <?php echo $this->Html->link($this->Html->image('logo.png'), array('controller' => 'main', 'action' => 'homepage'), array('escape' => false)); ?>
    </div>
    <div style="float:left;color: #D1E751;margin-top: 40px;">Azərbaycanın Ilk Milli Video Ensiklopediyası</div>
    <div id="headerright">

        <?php if (!isLoggedIn($this)): ?>
            <?php echo $this->Html->link('Facebook ilə giriş', array('controller' => 'users', 'action' => 'facebook')); ?>
            <?php echo $this->Html->link('Giriş', array('controller' => 'main', 'action' => 'login')); ?>
        <?php else: ?>
            <?php echo $this->Html->link('Çıxış', '/users/logout', array('class' => 'logoutlink')); ?>
            <a><?php echo "Xoş gördük, " . $this->Session->read('Auth.User.fullname'); ?></a>
        <?php endif; ?>

    </div>
    <div id="menu">


        <?php
        echo $this->Html->link('Gündəm', '/', array('class' => 'menuitemnormal'));
        echo $this->Html->link('Kanallar', '/kanallar', array('class' => 'menuitemnormal'));
        echo $this->Html->link('Proqramlar', '/proqramlar', array('class' => 'menuitemnormal'));
        echo $this->Html->link('Ustadlar', '/ustadlar', array('class' => 'menuitemnormal'));
        echo $this->Html->link('Bizdən soruşun', '/bizdensorush', array('class' => 'menuitemnormal'));
        ?>

        <?php
        echo $this->Html->link('', '/feed/allepisodes.rss', array('class' => 'menuright', 'id' => 'rss'));
        echo $this->Html->link('', '/elaqe', array('class' => 'menuright', 'id' => 'writeus'));
        ?>

        <div id="searchdiv">
            <form action="<?php echo Router::url(array('controller' => 'main', 'action' => 'search')); ?>" method="post" class="ignoredefault">
                <input type="submit" class="menuright ignoredefault" id="search" value="" /><input type="text" name="query" id="searchbox" class="ignoredefault"/>
            </form>
        </div>
    </div>

</div>
    
</div>