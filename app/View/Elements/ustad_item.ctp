<?php
$ustadphoto = Router::url('/files/photos/' . $ustad['photo']);
$ustadlink = Router::url(array('controller' => 'main', 'action' => 'ustad', 'id' => $ustad['id']));
?>
<div class="ustaditem lightgradient" <?php if(isset($float) && $float == 'right'){ echo 'style="float: right;margin: 0px;"';} ?>>
    <img class="uimg" src="<?php echo $ustadphoto; ?>"/>
    <a class="uname" href="<?php echo $ustadlink; ?>"><?php echo $ustad['fullname']; ?></a>
    <div class="upro"><?php echo $this->Html->link($ustad['profession'], array('controller'=>'main', 'action'=>'ustads', 'pro'=>$ustad['profession']), array('title'=>'Bu ixtisaslı bütün ustadları göstər')); ?></div>
</div>