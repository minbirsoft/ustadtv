<div class="cheader lightgradient">
    <div class="cname"><?php echo $mainchannel['Channel']['name']; ?></div>

    <?php
    foreach ($mainchannel['ChildChannels'] as $childchannel) {
        echo $this->Html->link($childchannel['name'], array('controller' => 'main', 'action' => 'channel', 'id' => $childchannel['id'], 'slug' => $childchannel['slug']), array('class' => 'altc'));
    }
    ?>

</div>