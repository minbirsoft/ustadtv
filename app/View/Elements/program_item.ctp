<?php
$programlink = Router::url(array('controller' => 'main', 'action' => 'program', 'id' => $item['Program']['id'], 'slug' => $item['Program']['slug']));
$programphoto = Router::url('/files/photos/' . $item['Program']['photo']);
$ustadlink = Router::url(array('controller' => 'main', 'action' => 'ustad', 'id' => $item['Ustad']['id']));
$channellink = Router::url(array('controller' => 'main', 'action' => 'channel', 'id' => $item['Channel']['id'], 'slug' => $item['Channel']['slug']));
?>
<div class="episodeitem lightgradient">
    <img class="ethumb" src="<?php echo $programphoto; ?>" />
    <div class="eside">
        <div class="esideinfo">
            <div class="etitle"><a href="<?php echo $programlink; ?>"><?php echo $item['Program']['title']; ?></a></div>
            <div class="eustad">Ustad: <a class="ustadlink" href="<?php echo $ustadlink; ?>"><?php echo $item['Ustad']['fullname']; ?></a></div>
            <div class="einfo"><?php echo mb_substr($item['Program']['description'], 0, 101); ?></div>
        </div>
        <div class="efooter">
            <a class="channellink" href="<?php echo $channellink; ?>"><?php echo mb_substr($item['Channel']['name'], 0, 20); ?></a>
            <div class="etimedate"><?php echo date('d.m.Y', strtotime($item['Program']['created'])); ?></div>
        </div>
    </div>
</div>