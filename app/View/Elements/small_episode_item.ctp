<?php
$episodelink = Router::url(array('controller' => 'main', 'action' => 'episode', 'id' => $item['Episode']['id'], 'slug' => $item['Episode']['slug']));
?>

<div class="smallepisodeitem lightgradient">
  <span><?php echo $number; ?></span>
  <a href="<?php echo $episodelink; ?>"><?php echo $item['Episode']['title'];?></a>
</div>