<?php //pr($this->Session->read('Message.flash.params.type'));
$type = $this->Session->read('Message.flash.params.type');
$layout = $type == 'error' ? 'center' : 'topCenter';
?>
<script type="text/javascript">
    $(document).ready(function() {
        
        var n = noty({
            text: '<?php echo $message; ?>',
            type: '<?php echo $type; ?>',
            dismissQueue: true,
            layout: '<?php echo $layout; ?>',
            theme: 'defaultTheme',
            <?php if($type == 'error') : ?>
            buttons: [
                {addClass: 'btn btn-primary', text: 'Yaxşı', onClick: function($noty) {

                        $noty.close();
                    }
                }
            ]
            <?php else: ?>
            timeout: 3000
            <?php endif; ?>
        });
    });
    
</script>