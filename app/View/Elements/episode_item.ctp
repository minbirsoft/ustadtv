<?php

$episodelink = Router::url(array('controller' => 'main', 'action' => 'episode', 'id' => $item['Episode']['id'], 'slug' => $item['Episode']['slug']));
$programlink = Router::url(array('controller' => 'main', 'action' => 'program', 'id' => $item['Program']['id'], 'slug' => $item['Program']['slug']));
$episodephoto = Router::url('/files/photos/' . $item['Episode']['photo']);

?>

<div class="episodeitem lightgradient">
	<div class="ethumb">
		<img src="<?php echo $episodephoto; ?>" />
		<div class="estats">
			<span title="Baxış sayı"><img src="./img/eye_view.png"/> <?php echo $item['Episode']['viewcount']; ?></span> <span title="Bəyənilmə sayı"><img src="./img/star_like.png"/> <?php echo $item['Episode']['likecount']; ?></span>
		</div>
	</div>
    
    <div class="eside">
        <div class="esideinfo">
            <div class="etitle"><a href="<?php echo $episodelink; ?>"><?php echo $item['Episode']['title']; ?></a></div>
            <div class="einfo"><?php echo mb_substr($item['Episode']['description'], 0, 101); ?></div>
        </div>
        <div class="efooter">
            <a class="ustadlink" href="<?php echo $programlink; ?>"><?php echo mb_substr($item['Program']['title'], 0,20); ?></a>
            <div class="etimedate"><?php echo date('d.m.Y', strtotime($item['Episode']['created'])); ?></div>
        </div>
    </div>
</div>