<div id="footer">
    <div id="footercontent" style="width:960px; margin:auto; height: 70px; text-align:left;">

        <?php
        echo $this->Html->link('Gündəm', '/', array('class' => 'footeritem'));
        echo $this->Html->link('Kanallar', '/kanallar', array('class' => 'footeritem'));
        echo $this->Html->link('Proqramlar', '/proqramlar', array('class' => 'footeritem'));
        echo $this->Html->link('Ustadlar', '/ustadlar', array('class' => 'footeritem'));
        echo $this->Html->link('Bizdən soruşun', '/bizdensorush', array('class' => 'footeritem'));
        echo $this->Html->link('Əlaqə', '/elaqe', array('class' => 'footeritem'));
        ?>

        <a href="http://twitter.com/ustadtv"><div class="socialnetwork" id="twitter"></div></a>
        <a href="http://www.facebook.com/USTADTV"><div class="socialnetwork" id="facebook"></div></a>
        <a href="https://plus.google.com/100279274981465627246/posts"><div class="socialnetwork" id="google"></div></a>
        <div class="footermail"> <a href="mailto:info@ustadtv.az">info@ustadtv.az</a> </div>

        <div id="copyright">© 2012 Ustad Tv. Bütün hüquqlar qorunur. Hazırladı: <a href="http://minbirsoft.com">MinbirSoft</a></div>

    </div>
</div><!-- footer div end -->