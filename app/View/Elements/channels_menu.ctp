<?php ?>
<div id="vertmenu">
    <div id="vertmenuheader">
        <?php echo $menutitle; ?>
    </div>
    <ul id="vertmenuitems">
        <?php foreach ($channels as $channel): ?> 
            <?php
            $url = Router::url(array(
                        'controller' => 'main',
                        'action' => 'channel',
                        'id' => $channel['Channel']['id'],
                        'slug' => $channel['Channel']['slug']
                    ));
            ?>

            <li class="vertmenuitem"> <a href="<?php echo $url; ?>"><?php echo $channel['Channel']['name']; ?></a> </li>
<?php endforeach; ?>

    </ul>
</div>      