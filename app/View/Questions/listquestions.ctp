<?php
$this->layout = 'admin';
?>
<h1>Sualların siyahısı</h1>
<table>
    <tr>
        <th>Nömrə</th>
        <th>Sual</th>
        <th>Soruşan</th>
        <th>Əməliyyat</th>
    </tr>

    <?php foreach ($questions as $question): ?>
    <tr>
        <td><?php echo $question['Question']['id']; ?></td>
        <td><?php echo $question['Question']['text']; ?></td>
        <td><?php echo $this->Html->link($question['User']['fullname'], array(
            'controller' => 'users',
            'action' => 'edituser',
            $question['Question']['user_id']
        )); 
            ?>
        </td>
        <td><?php echo $this->Form->postLink( 'Sil',
                          array('action' => 'deletequestion', $question['Question']['id']),
                          array('confirm' => 'Silmək istədiyinizə əminsiniz?')
                       ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($question); ?>
</table>
<?php
echo $this->Paginator->numbers(array(
    'before' => 'Səhifələmək: ',
    'first' => 2, 
    'last' => 2
    ));
?>