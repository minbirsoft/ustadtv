<?php
$this->layout = 'ustadtv';
?>

    <?php
    echo $this->element('header');
    ?>
<div id="content">


    <div id="body">
        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">


            <?php
            echo $this->element('right_header', array(
                'title' => 'Axtarış nəticələri',
                'link' => '',
                'extend' => ''
            ));
            ?>

            <?php
            if (count($results) > 0) {

                foreach ($results as $episode) {
                    echo $this->element('episode_item', array(
                        'item' => $episode
                    ));
                }
            } else {
                echo "Axtardığınız sözə uyğun heç bir nəticə tapılmadı. Başqa oxşar sözlərlə yenidən yoxlayın";
            }
            ?>    

            <?php
            if ($this->Paginator->params()['pageCount'] > 1) {
                $pagenumbers = $this->Paginator->numbers(array(
                    'first' => 2,
                    'last' => 2,
                    'tag' => 'span',
                    'separator' => ''
                        ));

                echo $this->element('paginator', array(
                    'numbers' => $pagenumbers
                ));
            }
            ?>


        </div>
    </div>
</div>

<?php
echo $this->element('footer');
?>    