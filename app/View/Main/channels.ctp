<?php
$this->layout = 'ustadtv';
?>
<?php
    echo $this->element('header');
    ?>
<div id="content">

    

    <div id="body">

        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">

            <div class="rightheader">
                Bütün Kanalların siyahısı
            </div>

            <?php
            //pr($lastprograms);

            foreach ($channels as $mainchannel) {
                echo $this->element('channel_map_item', array(
                    'mainchannel' => $mainchannel
                ));
            }
            ?>   



        </div>
        <div class="clearer"></div>
    </div>
    
</div>

<?php
echo $this->element('footer');
?>    
