<?php
$this->layout = 'ustadtv';

?>

 <?php
    echo $this->element('header');
 ?>

<div id="content">

   

    <div id="body">
        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">

            <?php
            echo $this->element('right_header', array(
                'title' => 'Ustadlara sual ver',
                'link' => '',
                'extend' => ''
            ));
            
            echo $this->Form->create('Question');
            echo "Ustadlara vermək istədiyiniz sualı yazın və göndərmək üçün soruş düyməsini basın. Aktual mövzu olarsa bu haqda proqram və ya epizod əlavə olunacaq <br/><br/>";
            echo $this->Form->input('text',  array('rows' => '3', 'label' => false));
            echo $this->Form->end('Soruş');
            
            
            
            ?>


        </div>
    </div>
    <div class="clearer"></div>
</div>

<?php
echo $this->element('footer');
?>    