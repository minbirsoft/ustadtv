<?php
$this->layout = 'ustadtv';
?>
 <?php
    echo $this->element('header');
    ?>
<div id="content">

   

    <div id="body">
        <div id="left">

            <?php
            //pr($childrenchannels);

            echo $this->element('channels_menu', array(
                'menutitle' => $channel['Channel']['name'],
                'channels' => $childrenchannels
            ));
            ?>

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Əsas Kanallar',
                'channels' => $parentchannels
            ));
            ?>

        </div>

        <div id="right">

            <?php
            echo $this->element('right_header', array(
                'title' => $channel['Channel']['name'] . ' kanalında ən son proqramlar',
                'link' => '',
                'extend' => ''
            ));
            ?>

            <?php
            
            if ($lastprograms) {
				foreach ($lastprograms as $program) {
					echo $this->element('program_item', array(
							'item' => $program
					));
				}
			} else {
				echo $this->element('error_message', array(
						'error' => "Bağışlayın, hal-hazırda bu kanala heç bir proqram əlavə edilməyib"
				));
			}

            
            ?>   



        </div>
    </div>

</div>

<?php
echo $this->element('footer');
?>    