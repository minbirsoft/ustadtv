<?php
$this->layout = 'ustadtv';
?>

    <?php
    echo $this->element('header');
    ?>
<div id="content">


    <div id="body">
        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">

            <?php
            echo $this->element('right_header', array(
                'title' => 'Hesaba giriş',
                'link' => '',
                'extend' => ''
            ));
            ?>

            <div style="float:left; width: 400px;padding-right: 25px; border-right: 2px solid #D1E751;">
                <?php
                echo $this->Form->create('LoginData', array('url' => array('controller' => 'users', 'action' => 'login')));
                echo $this->Form->input('username', array('label' => 'İstifadəçi adı və ya Email'));
                echo $this->Form->input('password', array('label' => 'Şifrə'));
                echo $this->Form->end('Daxil ol');
                ?>
            </div>

            <div style="float: right; width: 300px; "><br/>
                <h1>Hesabınız yoxdur?</h1> <br/><br/>
                <?php echo $this->Html->link('Facebook ilə daxil olun', array('controller' => 'users', 'action' => 'facebook'));?>
                <br/><br/>
                və ya <br/><br/>
                
                <?php echo $this->Html->link('Qeydiyyatdan keçin', array('controller' => 'main', 'action' => 'signup')) ?>
            </div>

        </div>
    </div>
    <div class="clearer"></div>
</div>

<?php
echo $this->element('footer');
?>    