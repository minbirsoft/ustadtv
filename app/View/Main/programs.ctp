<?php
$this->layout = 'ustadtv';
?>

    <?php
    echo $this->element('header');
    ?>
<div id="content">


    <div id="body">
        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">


            <?php
            echo $this->element('right_header', array(
                'title' => 'Bütün proqramlar',
                'link' => '',
                'extend' => ''
            ));
            ?>

            <?php
            foreach ($programs as $program) {
                echo $this->element('program_item', array(
                    'item' => $program
                ));
            }
            ?>    

            <?php
            if ($this->Paginator->params['pageCount'] > 1) {
                $pagenumbers = $this->Paginator->numbers(array(
                    'first' => 2,
                    'last' => 2,
                    'tag' => 'span',
                    'separator' => ''
                        ));

                echo $this->element('paginator', array(
                    'numbers' => $pagenumbers
                ));
            }
            ?>


        </div>
    </div>
</div>

<?php
echo $this->element('footer');
?>    