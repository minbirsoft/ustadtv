<?php
$this->layout = 'ustadtv';
?>

    <?php
    echo $this->element('header');
    ?>

<div id="content">

    <div id="body"> 

        <div id="left2">
            <div class="contentheader" style="float: none;"><div style="margin-left: 15px;">
                    <?php
                    echo $this->Html->link($program['Channel']['name'], array('controller' => 'main', 'action' => 'channel', 'id' => $program['Channel']['id'], 'slug' => $program['Channel']['slug']));
                    echo ' → ';
                    echo $program['Program']['title'];
                    ?> 
                </div>
            </div>

            <p>
                <img class="ethumb" src="<?php echo Router::url("/files/photos/" . $program['Program']['photo']); ?>" style="margin-right: 20px; float: left;" />
            <div style="float: right;border-bottom:2px solid #d1e751; color: #26aee5; font-size: 15px; width: 595px; margin-bottom: 20px;"><?php echo $program['Program']['title']; ?> </div>
            <?php echo $program['Program']['description']; ?> 
            </p>

            <div style="float: left;border-bottom:2px solid #d1e751; color: #26aee5; font-size: 15px; width: 750px; clear:both; margin: 20px 0 20px 0;">Bu proqramdakı cavablar</div>


            <div id="programwrapper">

                <?php foreach ($program['Episodes'] as $episode): ?>

                    <div class="episodeitem lightgradient" style="width:700px; background-color: #fff; float:none;">
                        <img class="ethumb" src="<?php echo Router::url("/files/photos/" . $episode['photo']); ?>" />
                        <div class="eside" style="width:568px;"">
                            <div class="esideinfo" style="width:550px;">
                                <div class="etitle" style="width:550px;">
                                    <?php echo $this->Html->link($episode['title'], array('controller'=>'main', 'action'=>'episode', 'id' => $episode['id'], 'slug'=>$episode['slug']));?>
                                </div>
                                <div class="einfo" style="clear: both; width:550px;"><?php echo $episode['description'];?></div>
                            </div>
                            <div class="efooter" style="width:550px;">
                                <?php echo $this->Html->link($program['Ustad']['fullname'], array('controller' => 'main', 'action' => 'ustad', 'id' => $program['Ustad']['id']), array('class'=>'ustadlink')); ?>
                                <div class="etimedate"><?php echo date('d.m.Y', strtotime($episode['created'])); ?></div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
                <div class="clearer"></div>
            </div>

        </div>
        <div id="right2">

            <?php
            echo $this->element('ustad_item', array(
                'ustad' => $program['Ustad']
            ));
            ?>

            <div id="tags" style="float: right; width: 200px; color: #828282">
                <div style="float: right;border-bottom:2px solid #d1e751; color: #26aee5; font-size: 15px; width: 200px; ">Bu Proqrama aid Açar sözlər</div>
                <?php echo $program['Program']['tags']; ?> 
            </div>
        </div>


        <div class="clearer"></div>
    </div>

</div>

<?php
echo $this->element('footer');
?>    