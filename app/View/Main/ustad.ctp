<?php
$this->layout = 'ustadtv';
?>

    <?php
    echo $this->element('header');
    ?>
<div id="content">




    <div id="body">
        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">  

            <div id="ustadwrapper">
                <img id="ustadprofileimg" src="<?php echo Router::url('/files/photos/' . $ustad['Ustad']['photo']); ?>" />
                <div id="ustadprofilename"><?php echo $ustad['Ustad']['fullname']; ?></div>
                <div id="ustadprofilepro"><?php echo $this->Html->link($ustad['Ustad']['profession'], array('controller'=>'main', 'action'=>'ustads', 'pro'=>$ustad['Ustad']['profession']), array('title'=>'Bu ixtisaslı bütün ustadları göstər')); ?></div>
                <div id="ustadprofileportfolio" class="lightgradient">
                    <div class="portfoliosection">
                        <div class="count"><?php echo $stats['programscount']; ?></div>
                        <div class="countname">proqram</div>
                    </div>

                    <div class="portfoliosection">
                        <div class="count"><?php echo $stats['episodescount']; ?></div>
                        <div class="countname">cavab</div>
                    </div>

                    <div class="portfoliosection" style="border-right: 0px;">
                        <div class="count"><?php echo $stats['viewcount']; ?></div>
                        <div class="countname">izlənmə</div>
                    </div>
                </div>
                <div class="clearer"></div>

                <div id="ustadprofileinfoheader" style="border-bottom: 2px solid #d1e751;color:#828282; margin-top: 30px;">
                    Ustad Haqqında geniş məlumat
                </div>
                <div id="ustadprofileinfo" class="lightgradient" style="margin-bottom: 25px;border-radius: 0 0 5px 5px;background-color: #fff;border:1px solid #ccc;border-top:0px;color:#444; padding: 15px 10px 5px 10px;font-size: 14px;">
                    <?php echo $ustad['Ustad']['info']; ?>
                    <div class="clearer"></div>
                </div>
            </div>


            <?php
            echo $this->element('right_header', array(
                'title' => 'Ustadın proqramları',
                'link' => '',
                'extend' => ''
            ));
            ?>

            <?php
            foreach ($programs as $program) {
                echo $this->element('program_item', array(
                    'item' => $program
                ));
            }
            ?>   


        </div>
    </div>

</div>

<?php
echo $this->element('footer');
?>    