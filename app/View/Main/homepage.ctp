<?php
$this->layout = 'ustadtv';
echo $this->Html->css('slider-style.css');
echo $this->Html->script('jquery.aw-showcase.js');
?>
<?php
	echo $this->element('header');
?>
<div id="content">

	

	<div id="body">

		<div style="width: 960px; height: 400px;">


			<div id="showcase" class="showcase" style="width: 560px;">

				<?php foreach ($last3slides as $episode): 
				$url = Router::url(array('controller'=>'main', 'action'=>'episode','id'=>$episode['Episode']['id'], 'slug'=>$episode['Episode']['slug']));
				$episodephoto = Router::url('/files/photos/' . $episode['Episode']['photo']);
				$episodesliderphoto = Router::url('/files/photos/' . $episode['Episode']['slider_photo']);
				?>
				<div class="showcase-slide">

					<div class="showcase-content">
						<a href="<?php echo $url; ?>"><img src="<?php echo $episodesliderphoto; ?>" /> </a>
					</div>

					<div class="showcase-thumbnail">
						<img src="<?php echo $episodephoto; ?>" width="100px" />
						<div class="showcase-thumbnail-caption">
							<?php echo $episode['Episode']['title']; ?>
						</div>
						<div class="showcase-thumbnail-cover"></div>
					</div>
					<!-- Put the caption content in a div with the class .showcase-caption -->
					<div class="showcase-caption">
						<h2>
							<a class="slidertitle" href="<?php echo $url; ?>"><?php echo $episode['Episode']['title']; ?>
							</a>
						</h2>
						<p>
							<?php echo $episode['Episode']['description']; ?>
						</p>
					</div>
				</div>

				<?php endforeach; ?>
			</div>



			<script>
                jQuery(document).ready(function(e) {
                    
                    $("#showcase").awShowcase(
						{
							content_width:			560,
							content_height:			376,
							fit_to_parent:			false,
							auto:					true,
							interval:				3000,
							continuous:				true,
							loading:				true,
							tooltip_width:			200,
							tooltip_icon_width:		32,
							tooltip_icon_height:		32,
							tooltip_offsetx:		18,
							tooltip_offsety:		0,
							arrows:					false,
							buttons:				false,
							btn_numbers:			false,
							keybord_keys:			true,
							mousetrace:				false,
							pauseonover:			true,
							stoponclick:			false,
							transition:				'hslide',
							transition_delay:		300,
							transition_speed:		500,
							show_caption:			'show',
							thumbnails:				true,
							thumbnails_position:	'outside-last',
							thumbnails_direction:	'vertical',
							thumbnails_slidex:		1,
							dynamic_height:			false,
							speed_change:			true,
							viewline:				false
						});
                });
            </script>

			<div id="homepagemainright">
				<div class="homepagemainrightheader">Ən çox baxılanlar</div>

				<?php 
				$number = 1;
				foreach($mostviewed3 as $episode) {
				    echo $this->element('small_episode_item', array(
						'item' => $episode,
						'number' => $number
				    ));
				    $number++;
				    unset($episode);
                }

                  ?>

				<div class="homepagemainrightheader">Ən çox bəyənilənlər</div>

				<?php 
				$number = 1;
				foreach($mostliked3 as $episode){
			    	echo $this->element('small_episode_item', array(
						'item' => $episode,
						'number' => $number
				    ));
				    $number++;
				    unset($episode);
                }

                  ?>

			</div>
                    

		</div>
		<div id="left">

			<?php
			echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>
            
            <?php
			//echo $left_sidebar_widget;
			?>

		</div>

		<div id="right">
			<?php
			echo $this->element('right_header', array(
                'title' => 'Ən son cavablar',
                'link' => '',
                'extend' => ''
            ));
            ?>

			<?php
			foreach ($lastepisodes as $episode) {
                echo $this->element('episode_item', array(
                    'item' => $episode
                ));
            }
            ?>

			<?php
			echo $this->element('right_header', array(
                'title' => 'Ən son proqramlar',
                'link' => Router::url(array('controller' => 'main', 'action' => 'programs')),
                'extend' => 'Bütün proqramlar'
            ));
            ?>

			<?php
			foreach ($lastprograms as $program) {
                echo $this->element('program_item', array(
                    'item' => $program
                ));
            }
            ?>


			<?php
			echo $this->element('right_header', array(
                'title' => 'Ustadlar',
                'link' => Router::url(array('controller' => 'main', 'action' => 'ustads')),
                'extend' => 'Bütün ustadlar'
            ));
            ?>

			<?php
			foreach ($someustads as $ustad) {
                echo $this->element('ustad_item', array(
                    'ustad' => $ustad['Ustad']
                ));
            }
            ?>

		</div>
		<div class="clearer"></div>
	</div>
	<div class="clearer"></div>
</div>

<?php
echo $this->element('footer');
?>