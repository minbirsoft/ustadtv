<?php
$this->layout = 'ustadtv';
?>

    <?php
    echo $this->element('header');
    ?>
<div id="content">


    <div id="body">
        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">

            <?php
            echo $this->element('right_header', array(
                'title' => 'Qeydiyyat',
                'link' => '',
                'extend' => ''
            ));

            echo $this->Form->create('User', array('url' => array('controller' => 'main', 'action'=>'signup')));
            echo $this->Form->input('User.fullname', array('label' => 'Adınız, soyadınız'));
            echo $this->Form->input('User.email', array('label' => 'Emailiniz'));
            echo $this->Form->input('User.username', array('label' => 'İstifadəçi adınız'));
            echo $this->Form->input('User.password', array('label' => 'Şifrəniz'));
            echo $this->Form->end('Qeydiyyatdan keç');
            ?>


        </div>
    </div>
    <div class="clearer"></div>
</div>

<?php
echo $this->element('footer');
?>    