<?php
$this->layout = 'ustadtv';

echo $this->Html->css('playertheme/style.css');
echo $this->Html->script('projekktor-1.2.05r145.min.js');

echo $this->Html->meta('canonical', $this->Html->url(null, true), array('rel' => 'canonical', 'type' => null, 'title' => null, 'inline' => false));
?>

	<?php
	echo $this->element('header');
	?>
<div id="content">


	<div id="body">
		<div class="contentheader">
			<div style="margin-left: 15px;">
				<?php
				echo $this->Html->link($episode['Program']['Channel']['name'], array('controller' => 'main', 'action' => 'channel', 'id' => $episode['Program']['Channel']['id'], 'slug' => $episode['Program']['Channel']['slug']));
				echo ' → ';
				echo $this->Html->link($episode['Program']['title'], array('controller' => 'main', 'action' => 'program', 'id' => $episode['Program']['id'], 'slug' => $episode['Program']['slug']));
				echo ' → ';
				echo $episode['Episode']['title'];
				?>
			</div>
		</div>
		<div class="clearer"></div>

		<div style="width: 955px; color: #666; padding-left: 5px;">
			<?php echo $episode['Episode']['title'] ?>
		</div>

		<div id="episodewrapper">

			<div id="video">

				<video id="player" class="projekktor"
					title="<?php echo $episode['Episode']['title']; ?>" width="640"
					height="360">

<script type="text/javascript">
$(document).ready(function() {
	projekktor('#player',
		{
			controls: true,
			autoplay: true,
			playlist: [
				<?php if (isset($episode['Adversitement']['id'])): ?>
		    	{
		    		0:{src:'<?php echo $episode['Adversitement']['video']; ?>', type: 'video/youtube'},
		     		config: {
		      			disablePause: true,
		      			disallowSkip: true,
		      			title: 'Reklam'
		      		}
		    	}, <?php endif; ?>
		    	{
		    		0:{src:'<?php echo $episode['Episode']['video']; ?>', type: 'video/youtube'}
		    		}
		   	]
            },
            function(player) {
            	$('#projekktorver').html( player.getPlayerVer() );
            }
	    );
	});
</script>
			
			</div>

			<div id="episodeustad" class="lightgradient">
				<?php echo $this->Html->link($this->Html->image('/files/photos/' . $episode['Program']['Ustad']['photo']), '#', array('escape'=>false)); ?>
				<div id="episodeustadname">
					<?php echo $episode['Program']['Ustad']['fullname']; ?>
				</div>
				<div id="episodeustadprofession">
					<?php echo $episode['Program']['Ustad']['profession']; ?>
				</div>
				<div id="episodeustadinfo">
					<?php echo $episode['Program']['Ustad']['info']; ?>
				</div>
			</div>


			<div id="tags">
				<div
					style="border-bottom: 2px solid #d1e751; color: #26aee5; font-size: 15px;">Açar
					sözlər</div>
				<?php
				echo $episode['Episode']['tags'];
				?>
			</div>

			<div id="videostats" style="margin-bottom: 5px; clear: left;">
				<button id="episodelike"
					class="<?php if($hasliked){ echo "hasliked";} else {echo "hasnotliked";} ?>">
					<?php if($hasliked) { 
							echo "Bunu bəyənirsiniz";
						} else {echo "Bəyən";
					} ?>
				</button>
				<?php echo $episode['Episode']['viewcount']; ?>
				dəfə baxılıb,
				<?php echo $episode['Episode']['likecount']; ?>
				nəfər tərəfindən bəyənilib.

				<script>
            		$("#episodelike").click(function(){
						likebuttonclickevent(<?php echo $episode['Episode']['id'];?>);
					});
            	</script>
			</div>

			<div id="share">

				<div id="paylasmaq" style="clear: left; color: #fff; height: 20px;">Dostlarınla
					paylaş</div>

				<div style="margin-right: 15px;">
					<fb:like send="false" layout="button_count" width="450"
						show_faces="true"></fb:like>
				</div>
				<div>
					<a
						href="https://twitter.com/share?url=<?php echo $this->Html->url(null, true); ?>"
						class="twitter-share-button" data-via="ustadtv" data-lang="en">Tvitlə</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
				<div>
					<iframe id="rastplus-button-iframe" frameborder=0 width="80px"
						height="20px"></iframe>
					<script>
                        var rastplusbtn=document.getElementById("rastplus-button-iframe");
                        rastplusbtn.src = "http://rastlash.com/rastplus/plugin/?type=button&url="+encodeURIComponent(document.URL);
                    </script>
				</div>
				<div>
					<div class="g-plus" data-action="share" data-annotation="bubble"></div>
					<script type="text/javascript">
                        (function() {
                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                            po.src = 'https://apis.google.com/js/plusone.js';
                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                        })();
                    </script>
				</div>
			</div>

			<div id="listepisodes">
				<div
					style="border-bottom: 2px solid #d1e751; color: #26aee5; font-size: 15px;">
					<?php echo $episode['Program']['title'] . " proqramından digər cavablar"; ?>
				</div>
				<?php
				foreach ($episode['Program']['Episodes'] as $otherepisode) {
                    echo $this->Html->link($otherepisode['title'], array(
                        'controller' => 'main',
                        'action' => 'episode',
                        'id' => $otherepisode['id'],
                        'slug' => $otherepisode['slug']
                    ));
                }
                ?>
			</div>

			<div class="clearer"></div>
		</div>

		<div id="comments" style="float: left; width: 640px; margin-top: 20px;">
			<div style="border-bottom: 2px solid #d1e751; color: #26aee5; font-size: 15px;">Şərh yazın</div>
			<fb:comments href="<?php echo $this->Html->url(null, true);?>" width="640" num_posts="10"></fb:comments>
		</div>

	</div>

</div>

<?php
echo $this->element('footer');
?>
