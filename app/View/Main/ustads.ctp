<?php
$this->layout = 'ustadtv';
?>

    <?php
    echo $this->element('header');
    ?>

<div id="content">



    <div id="body">
        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">  


            <?php
            
            $headertitle = "Ustadlar";
            
            if(isset($pro)){
            	$headertitle = "Ustadlar → İxtisas: ".$pro;
            }
            
            echo $this->element('right_header', array(
                'title' => $headertitle,
                'link' => '',
                'extend' => ''
            ));
            ?>

            <?php
            foreach ($ustads as $ustad) {
                echo $this->element('ustad_item', array(
                    'ustad' => $ustad['Ustad']
                ));
            }
            ?> 

            <?php
            $paginatiorparams = $this->Paginator->params();
            if ($paginatiorparams['pageCount'] > 1) {
                $pagenumbers = $this->Paginator->numbers(array(
                    'first' => 2,
                    'last' => 2,
                    'tag' => 'span',
                    'separator' => ''
                        ));

                echo $this->element('paginator', array(
                    'numbers' => $pagenumbers
                ));
            }
            ?>

        </div><div class="clearer"></div>
    </div><div class="clearer"></div>

</div>

<?php
echo $this->element('footer');
?>    