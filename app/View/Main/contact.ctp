<?php
$this->layout = 'ustadtv';
?>
<?php
    echo $this->element('header');
    ?>
<div id="content">

    

    <div id="body">
        <div id="left">

            <?php
            echo $this->element('channels_menu', array(
                'menutitle' => 'Kanallar',
                'channels' => $channels
            ));
            ?>

        </div>

        <div id="right">

            <?php
            echo $this->element('right_header', array(
                'title' => 'Əlaqə',
                'link' => '',
                'extend' => ''
            ));


            echo $this->Form->create('ContactData', array('url' => array('controller' => 'main', 'action' => 'contact')));
            echo $this->Form->input('fullname', array('label' => 'Adınız Soyadınız', 'default' => $fullname));
            echo $this->Form->input('email', array('label' => 'Email-iniz', 'default' => $email));
            echo $this->Form->input('text', array('label' => 'Bizə ismarışınız', 'rows' => '5'));
            echo $this->Form->end('Göndər');
            ?>


        </div>
    </div>
    <div class="clearer"></div>
</div>

<?php
echo $this->element('footer');
?>    