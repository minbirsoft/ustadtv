<?php
$this->layout = 'admin';
echo $this->Html->script('jquery-1.8.3.min');
echo $this->Html->script('jquery.uploadify.min');
echo $this->Html->script('uploadifyscript');
echo $this->Html->css('uploadify');
?>


<?php $timestamp = time(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        var timestamp = '<?php echo $timestamp; ?>';
        var token = '<?php echo md5('ustadtv_salt' . $timestamp); ?>';
        uploadifyIt('photo_upload', 'UstadPhoto', 'Şəkil seç', 'photos', timestamp, token , '*.jpg;*.png', 'ustad');
    });
</script>

<h1>Ustad məlumatlarına düzəliş et</h1>
<?php
echo $this->Form->create('Post');
echo $this->Form->input('Ustad.fullname');
echo $this->Form->input('Ustad.profession');
echo $this->Form->input('Ustad.info', array('rows' => '2'));
echo $this->Form->input('Ustad.photo');
?>

Şəklin yüklənilməsi: 
<input type="file" name="photo_upload" id="photo_upload" />

<?php
echo $this->Form->end('Yadda saxla');
?>