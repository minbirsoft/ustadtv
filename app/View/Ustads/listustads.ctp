<?php
$this->layout = 'admin';
?>
<h1>Ustadların siyahısı</h1>
<p><?php echo $this->Html->link("Yeni Ustad", array('action' => 'addustad')); ?></p>
<table>
    <tr>
        <th>Nömrə</th>
        <th>Ad, Soyad</th>
        <th>Əməliyyat</th>
    </tr>

    <?php foreach ($ustads as $ustad): ?>
    <tr>
        <td><?php echo $ustad['Ustad']['id']; ?></td>
        <td><?php echo $ustad['Ustad']['fullname']; ?></td>
        <td><?php echo $this->Html->link('Düzəliş et', array('action' => 'editustad', $ustad['Ustad']['id'])); ?>
            <?php echo $this->Form->postLink( 'Sil',
                          array('action' => 'deleteustad', $ustad['Ustad']['id']),
                          array('confirm' => 'Silmək istədiyinizə əminsiniz?')
                       ); ?>
            <?php echo $this->Html->link('Proqramlar', array(
                'controller' => 'programs',
                'action' => 'listprograms',
                '?' => array('listby'=>'ustad', 'withid'=>$ustad['Ustad']['id'])
                )); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($ustads); ?>
</table>