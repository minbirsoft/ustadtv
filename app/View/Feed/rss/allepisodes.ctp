<?php

$channel = array(
    'title' => 'UstadTV-də ən son epizodlar',
    'link' => $this->Html->url('/', true),
    'description' => 'UstadTV-ə əlavə olunan ən son epizodlardan anında xəbərdan olmaq üçün abunə olun',
    'language' => 'az-AZ'
);

$this->set('channel', $channel);

foreach ($posts as $post) {
    
    $postTime = strtotime($post['Episode']['created']);

    
    $postLink = array(
        'controller' => 'main',
        'action' => 'episode',
        'id' => $post['Episode']['id'],
        'slug' => $post['Episode']['slug']
    );
    
    
    echo  $this->Rss->item(array(), array(
        'title' => $post['Episode']['title'],
        'link' => $postLink,
        'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
        'description' => $post['Episode']['description'],
        'pubDate' => $post['Episode']['created']
    ));
    
}

?>