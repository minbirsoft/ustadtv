<?php
$this->layout = 'admin';
?>
<h1>Proqramların siyahısı</h1>
<p><?php echo $this->Html->link("Yeni Proqram", array('action' => 'addprogram')); ?></p>
<table>
    <tr>
        <th>Nömrə</th>
        <th>Kanal</th>
        <th>Ustad</th>
        <th>Başlıq</th>
        <th>Əməliyyat</th>
    </tr>

    <?php foreach ($programs as $program): ?>
    <tr>
        <td><?php echo $program['Program']['id']; ?></td>
        <td><?php echo $program['Program']['channel_id']; ?></td>
        <td><?php echo $program['Program']['ustad_id']; ?></td>
        <td><?php echo $program['Program']['title']; ?></td>
        <td><?php echo $this->Html->link('Düzəliş et', array('action' => 'editprogram', $program['Program']['id'])); ?>
            <?php echo $this->Form->postLink( 'Sil',
                          array('action' => 'deleteprogram', $program['Program']['id']),
                          array('confirm' => 'Silmək istədiyinizə əminsiniz?')
                       ); ?>
            <?php echo $this->Html->link('Cavablar', array(
                'controller' => 'episodes',
                'action' => 'listepisodes', 
                '?' => array('listby'=>'program', 'withid' => $program['Program']['id']))); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($program); ?>
</table>
<?php
echo $this->Paginator->numbers(array(
    'before' => 'Səhifələmək: ',
    'first' => 2, 
    'last' => 2
    ));
?>