<?php
$this->layout = 'admin';
echo $this->Html->script('jquery-1.8.3.min');
echo $this->Html->script('jquery.uploadify.min');
echo $this->Html->script('uploadifyscript');
echo $this->Html->css('uploadify');
?>


<?php $timestamp = time(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        var timestamp = '<?php echo $timestamp; ?>';
        var token = '<?php echo md5('ustadtv_salt' . $timestamp); ?>';
        uploadifyIt('photo_upload', 'ProgramPhoto', 'Şəkil seç', 'photos', timestamp, token , '*.jpg;*.png', 'program');
    });
</script>

<h1>Proqram Əlavə et</h1>
<?php
echo $this->Form->create('Post');
echo $this->Form->input('Program.title');
echo $this->Form->input('Program.slug');
echo $this->Form->input('Program.description', array('rows' => '2'));
echo $this->Form->input('Program.channel_id', array('type'=>'select','options'=>$channels));
echo $this->Form->input('Program.ustad_id', array('type'=>'select','options'=>$ustads));
echo $this->Form->input('Program.tags', array('rows' => '2'));
echo $this->Form->input('Program.photo');
?>

Şəklin yüklənilməsi: 
<input type="file" name="photo_upload" id="photo_upload" />

<?php
echo $this->Form->end('Əlavə et');
?>