<?php

App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {
	
	public $hasAndBelongsToMany = array(
			'Episode' =>
			array(
					'className' => 'Episode',
					'joinTable' => 'users_like_episodes',
					'foreignKey' => 'user_id',
					'associationForeignKey' => 'episode_id',
					'unique' => true
			)
	);
    
    public $validate = array(
        'username' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Bu istifadəçi adı ilə artıq qeydiyyatdan keçilib'
                
            ),
            'notEmpty' => array(
                'rule' => array('minLength', 3),
                'message' => 'İstifadəçi adı minimum 3 simvoldan ibarət olmalıdır',
                'allowEmpty' => false
            )
        ),
        'email' => array(
            'email' => array(
                'rule' => 'email',
                'message' => 'Yazdığınız email düzgün deyil'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Bu email ilə artıq qeydiyyatdan keçilib'
            )
        ),
        'password' => array(
            'rule' => array('minLength', 5),
            'message' => 'Şifrə ən azı 5 simvoldan ibarət olmalıdır'
        )
    );
    
    public function hash($param) {
        return AuthComponent::password($param);
    }
    
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

    
    
    public function getWithUsernameAndPassword($username, $password) {
        
        $hashedPassword = AuthComponent::password($password);
        
        $user = $this -> find('first', array(
            'conditions' => array(
                
                'or' => array(
                    'User.username' => $username,
                    'User.email' => $username
                ),
                'User.password' => $hashedPassword
            )
        ));
        
        return $user;
        
    }
    
    public function getWithFacebookUid($uid) {
        $user = $this -> find('first', array(
            'conditions' => array(
                'User.facebook_uid' => $uid
            )
        ));
        return $user;
    }
    
    public function getWithEmail($email) {
    	$user = $this -> find('first', array(
    			'conditions' => array(
    					'User.email' => $email
    			)
    	));
    
    	return $user;
    
    }
    
    public function like($user_id, $episode_id) {
    	$this->Episode->updateAll(array('Episode.likecount'=>'Episode.likecount+1'), array('Episode.id'=>$episode_id));
    
    	//$table = "`ustadtv`.`users_like_episodes`";
    
    	//$this->query("INSERT INTO ".$table." (user_id, episode_id) VALUES ( ".$user_id.", ".$episode_id." )");
    
    }
    
    public function hasLiked($user_id, $episode_id) {    	
    	$table = "`ustadtv`.`users_like_episodes`";
    	$record = $this->query("SELECT id FROM ".$table." WHERE user_id = ".$user_id." AND episode_id = ".$episode_id);
    	If( $record ) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
}

?>
