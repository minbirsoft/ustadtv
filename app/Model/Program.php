<?php

class Program extends AppModel {

    public $belongsTo = array(
        'Ustad' => array(
            'className' => 'Ustad',
            'foreignKey' => 'ustad_id'
        ),
        'Channel' => array(
            'className' => 'Channel',
            'foreignKey' => 'channel_id'
        )
    );
    
    var $hasMany = array(
        'Episodes' => array(
            'className' => 'Episode',
            'foreignKey' => 'program_id' 
        )
    );

    public function getLastN($limit) {
        return $this->getLastNOffsetN($limit, 0);
    }

    public function getLastNOffsetN($limit, $offset) {
        return $this->find('all', array(
                    'order' => 'Program.created DESC',
                    'limit' => $limit,
                    'offset' => $offset,
                    'recursive' => 0
                ));
    }

    public function getLastNInChannelOffsetN($channel, $limit, $offset) {
        return $this->find('all', array(
                    'conditions' => array('Program.channel_id' => $channel),
                    'order' => 'Program.created DESC',
                    'limit' => $limit,
                    'offset' => $offset,
                    'recursive' => 0
                        )
        );
    }

    public function getLastNOfUstadOffsetN($ustad, $limit, $offset) {
        return $this->find('all', array(
                    'conditions' => array('Program.ustad_id' => $ustad),
                    'limit' => $limit,
                    'offset' => $offset,
                    'recursive' => 0
                ));
    }
    
    public function getLastNOfAMainChannel($limit, $channel) {
        
        $db = $this->getDataSource();
        
        $channels_table = $db->fullTableName($this->Channel);
        $ustads_table = $db->fullTableName($this->Ustad);
        $programs_table = $db->fullTableName($this);

        return $this->query("SELECT Program.*, Channel.*, Ustad.* FROM ".$programs_table." as Program LEFT JOIN ".$channels_table." as Channel ON(Program.channel_id = Channel.id) LEFT JOIN ".$ustads_table." as Ustad ON (Program.ustad_id = Ustad.id) WHERE Program.channel_id IN (SELECT Channel2.id FROM ".$channels_table." as Channel2 WHERE Channel2.parent_id = ".$channel.") LIMIT ".$limit);
    }

}

?>
