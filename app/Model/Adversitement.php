<?php

class Adversitement extends AppModel {

    public $hasAndBelongsToMany = array(
        'Channel' =>
        array(
            'className' => 'Channel',
            'joinTable' => 'adversitements_channels',
            'foreignKey' => 'adversitement_id',
            'associationForeignKey' => 'channel_id',
            'unique' => true
        )
    );

    public function getAdForChannel($channel) {

        $db = $this->getDataSource();

        $adversitements_table = $db->fullTableName($this);
        $adversitements_channels_table = "`ustadtv`.`adversitements_channels`";

        $ad = $this->query("SELECT Adversitement.* FROM " . $adversitements_channels_table . " as AdversitementChannel LEFT JOIN " . $adversitements_table . " as Adversitement ON(AdversitementChannel.adversitement_id = Adversitement.id) WHERE AdversitementChannel.channel_id = " . $channel . " ORDER BY rand() LIMIT 1");

        return isset($ad[0]) ? $ad[0] : null;
    }

}

?>
