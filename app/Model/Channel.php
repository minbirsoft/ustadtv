<?php

class Channel extends AppModel {
    
    var $belongsTo = array(
        'ParentChannel' =>
        array('className' => 'Channel',
            'foreignKey' => 'parent_id'
        ),
    );
    
    var $hasMany = array(
        'ChildChannels' => array(
            'className' => 'Channel',
            'foreignKey' => 'parent_id' 
        ),
        'Programs' => array(
            'className' => 'Program',
            'forreignKey' => 'channel_id'
        )
    );
    
    public function getAllMain() {
        return $this -> find('all', array(
            'conditions' => array('Channel.parent_id'=>NULL),
            'recursive' => 0
        ));
    }
    
    public function getAllMainList() {
        return $this -> find('list', array(
                'conditions' => array('Channel.parent_id'=>NULL)
        ));
    }
    
    public function getAllMainWithChildren() {
        return $this -> find('all', array(
            'conditions' => array('Channel.parent_id'=>NULL),
            'recursive' => 1
        ));
    }
    
    public function getChildrenOf($channel) {
        return $this -> find('all', array(
            'conditions' => array('Channel.parent_id'=>$channel),
            'recursive' => 0
        ));
    }
    
    public function getParentWithChildren($channelid) {
        
        $channel = $this ->  find('first', array(
            'conditions' => array('Channel.id' => $channelid),
            'recursive' => 0
        ));
        
        $parentchannelid = $channelid;
        
        if(!empty($channel['Channel']['parent_id'])) {
            $parentchannelid = $channel['Channel']['parent_id'];
        }
        
        $parentchannel = $this ->  find('first', array(
            'conditions' => array('Channel.id' => $parentchannelid),
            'recursive' => 1
        ));
        
        return $parentchannel;
    }
    
    public function getChannelsGrouped() {
        
        $mainchannels = $this -> getAllMainWithChildren();
        $groupedchannels = array();
            
        foreach($mainchannels as $mainchannel){
            
            $childchannels = array();
            
            foreach($mainchannel['ChildChannels'] as $childchannel) {
                $childchannels[$childchannel['id']] = $childchannel['name'];
            }

            $groupedchannels[$mainchannel['Channel']['name']] = $childchannels;
            
        }
        
        return $groupedchannels;
            
    }

}

?>
