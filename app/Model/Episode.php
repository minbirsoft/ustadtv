<?php

class Episode extends AppModel {
    
    public $belongsTo = array(
        'Program' => array(
            'className' => 'Program',
            'foreignKey' => 'program_id'
        ),
        'Adversitement' => array(
            'className' => 'Adversitement',
            'foreignKey' => 'adversitement_id'
        )
    );
    
       
    public function getLastN($limit) {
        return $this->getLastNOffsetN($limit, 0);
    }

    public function getLastNOffsetN($limit, $offset) {
        return $this->find('all', array(
                    'order' => 'Episode.created DESC',
                    'limit' => $limit,
                    'offset' => $offset
                ));
    }

    public function getLastNInProgramOffsetN($program, $limit, $offset) {
        return $this->find('all', array(
                    'conditions' => array('Episode.program_id' => $program),
                    'order' => 'Episode.created DESC',
                    'limit' => $limit,
                    'offset' => $offset
                        )
        );
    }
    
    public function get3Slides() {
        return $this->find('all', array(
                    'conditions' => array('not' => array('Episode.slider_photo' => 'null')),
                    'order' => 'RAND()',
                    'limit' => 3
                        )
        );
    }
    
    
    
}

?>
