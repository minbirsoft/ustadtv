<?php

class Ustad extends AppModel {

    public $hasMany = array(
        'Program' => array(
            'className' => 'Program',
            'foreignKey' => 'ustad_id',
            'order' => 'Program.created DESC',
            'limit' => '5',
            'dependent' => true
        )
    );

    public function getNRandom($limit) {
        return $this->find('all', array(
                    'order' => 'RAND()',
                    'limit' => $limit,
                    'recursive' => 0
                ));
    }

    public function getNUstadsOfChannel($limit, $channel) {

        return $this->Program->find('all', array(
                    'conditions' => array('Program.channel_id' => $channel),
                    'group' => 'Program.ustad_id',
                    'limit' => $limit,
                    'recursive' => 0
                ));
    }

    public function getStats() {
        $sql = "SELECT " .
                "   COUNT(Episode.id) as episodescount, SUM(Episode.viewcount) as viewcount, COUNT(DISTINCT Episode.program_id) as programscount  " .
                "FROM " .
                "   `ustadtv`.`episodes` as Episode " .
                "WHERE " .
                "   Episode.program_id IN (SELECT Program2.id FROM `ustadtv`.`programs` as Program2 WHERE Program2.ustad_id = 2) ";

        $result = $this->query($sql);
        return $result[0][0];
    }

}

?>
