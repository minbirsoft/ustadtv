<?php

class SitemapController extends AppController {
    
    public $components = array('RequestHandler');

    public function sitemap() {
        
        $this->loadModel('Program');
        $this->loadModel('Episode');
        $this->loadModel('Ustad');
        $this->loadModel('Channel');
        
        //Configure::write('debug', 0);

        $episodes = $this->Episode->find('all', array('recursive'=>0));
        $programs = $this->Program->find('all', array('recursive'=>0));
        $ustads = $this->Ustad->find('all', array('recursive'=>0));
        $channels = $this->Channel->find('all', array('recursive'=>0));

        $this->set(compact('episodes'));
        $this->set(compact('programs'));
        $this->set(compact('ustads'));
        $this->set(compact('channels'));
        
        $this->RequestHandler->respondAs('xml');
    }

}

?>
