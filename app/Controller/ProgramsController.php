<?php


class ProgramsController extends AppController {
    
    /* Administrator actions*/ 
    
    public function listprograms($channelid = null) {
        
        onlyAdmin($this);
        
        $listby = 'none';
        $withid = 0;
        
        if (isset($this->params['url']['listby'])) {
            $listby = $this->params['url']['listby'];
            $withid = $this->params['url']['withid'];
        }
        
        if ('channel' == $listby) {
            $conditions = array('Program.channel_id' => $withid);
        } else if ('ustad' == $listby) {
            $conditions = array('Program.ustad_id' => $withid);
        } else {
            $conditions = array();
        }
        
        $this->paginate = array(
            'limit' => 30,
            'order' => array(
                'Program.id' => 'DESC'
            ),
            'conditions' => $conditions
        );
        
        $data = $this->paginate('Program');
           
        $this->set('programs', $data);
    }
    
    public function addprogram() {
        
        onlyAdmin($this);
        
         $this ->loadModel('Channel');
         $this ->loadModel('Ustad');
          
        if($this -> request -> is("post")) {
            
            $this->Program->create();
            if ($this->Program->save($this->request->data)) {
                $this->Session->setFlash('Proqram əlavə edildi.');
                $this->redirect(array('action' => 'listprograms'));
            } else {
                $this->Session->setFlash('Proqram əlavə edilə bilmədi');
            }
            
        } else {
            
            $channels = $this -> Channel -> find('list');
            $ustads = $this -> Ustad -> find('list', array(
                'fields' => array('Ustad.id', 'Ustad.fullname')
            ));
            $this->set('channels', $channels);
            $this->set('ustads', $ustads);
            
        }
    }
    
    public function editprogram($id) {
        
        onlyAdmin($this);
        
        $this ->loadModel('Channel');
        $this ->loadModel('Ustad');
         
        $this->Program->id = $id;
        if ($this->request->is('get')) {
            
            $channels = $this -> Channel -> find('list');
            $ustads = $this -> Ustad -> find('list', array(
                'fields' => array('Ustad.id', 'Ustad.fullname')
            ));
            $this->set(compact('channels'));
            $this->set(compact('ustads'));
            $this->request->data = $this->Program->read();
            
        } else {
            
            if ($this->Program->save($this->request->data)) {
                $this->Session->setFlash('Dəyişiklik saxlanıldı');
                $this->redirect(array('action' => 'listprograms'));
            } else {
                $this->Session->setFlash('Dəyişiklik saxlanıla bilmədi.');
            }
            
        }
    }
    
    public function deleteprogram($id) {
        
        onlyAdmin($this);
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Program->delete($id)) {
            
            $this->Session->setFlash($id . ' nömrəli proqram silindi.');
            $this->redirect(array('action' => 'listprograms'));
            
        }
    }
    
}

?>
