<?php

class AdminstratorController extends AppController{
    
    var $components= array('Session');
    
    public function session() {
        $this->autoRender = false;
        
        pr($this -> Session -> read());
    }
    
    public function panel() {
        onlyAdmin($this);
    }
    
    public function login() {
        
        $username = "admin";
        $password = "123456";
        
        if($this -> request -> is("post")) {
            
            if( ($this -> data['LoginData']['username'] == $username) && ($this -> data['LoginData']['password'] == $password)) {
                $this->Session->write('isAdmin', 'true');
                return $this->redirect('/adminstrator/panel/');
            } else {
                $this->Session->setFlash('Giriş detalları düzgün deyil');
            }
            
        }
        
    }
    
    public function logout() {
        $this->Session->delete('isAdmin');
        return $this->redirect('/adminstrator/login/');
    }
    
}

?>
