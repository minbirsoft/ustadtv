<?php


class AdversitementsController extends AppController {
    

    /* Administrator actions*/ 
    
    public function listadversitements() {
        
        onlyAdmin($this);
        
        $adversitements = $this -> Adversitement -> find('all');
        
           
        $this->set('adversitements', $adversitements);
    }
    
    public function addadversitement() {
        
        onlyAdmin($this);
        
        //$this ->loadModel('Channel');
          
        if($this -> request -> is("post")) {
            $this->Adversitement->create();
            if ($this->Adversitement->save($this->request->data)) {
                $this->Session->setFlash('Reklam əlavə edildi.');
                $this->redirect(array('action' => 'listadversitements'));
            } else {
                $this->Session->setFlash('Reklam əlavə edilə bilmədi');
            }
        } else {
            //$channelsgrouped = $this -> Channel -> getChannelsGrouped();
            //$this -> set('channels', $channelsgrouped);
        }
    }
    
    public function editadversitement($id) {
        
        onlyAdmin($this);
        
        //$this ->loadModel('Channel');
         
        $this->Adversitement->id = $id;
        if ($this->request->is('get')) {
            
            //$channelsgrouped = $this -> Channel -> getChannelsGrouped();
            //$this -> set('channels', $channelsgrouped);
            
            $this->request->data = $this->Adversitement->read();
            
        } else {
            
            if ($this->Adversitement->save($this->request->data)) {
                $this->Session->setFlash('Dəyişiklik saxlanıldı');
                $this->redirect(array('action' => 'listadversitements'));
            } else {
                $this->Session->setFlash('Dəyişiklik saxlanıla bilmədi.');
            }
        }
    }
    
    public function deleteadversitement($id) {
        
        onlyAdmin($this);
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Adversitement->delete($id)) {
            $this->Session->setFlash($id . ' nömrəli reklam silindi.');
            $this->redirect(array('action' => 'listadversitements'));
        }
    }
}

?>
