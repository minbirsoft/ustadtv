<?php

App::import('Vendor', 'resize-class');

class UploadController extends AppController {

    public function ajaxUpload() {
        $this->autoRender = false;

        $verifyToken = md5('ustadtv_salt' . $this->request->data('timestamp'));

        $folder = $this->request->data('folder');
        
        $name = $type = $size = $status = false;
        $message = 'There was a problem uploading the file';
        if (!empty($_FILES) && $this->request->data('token') == $verifyToken) {
            if ($_FILES['Filedata']['error'] == 0) {

                $allowedTypes = array(
                    'mp4',
                    'mpg',
                    'mpeg',
                    'mov',
                    'avi',
                    'mpv2',
                    'qt',
                    'flv',
                    'jpg',
                	'png'
                );
                $fileParts = pathinfo($_FILES['Filedata']['name']);
                $newFileName = md5($_FILES['Filedata']['name'] . time());
                if (in_array($fileParts['extension'], $allowedTypes)) {
                    $tempFile = $_FILES['Filedata']['tmp_name'];
                    $targetPath = WWW_ROOT . "/files/" . $folder . '/';
                    $targetFile = str_replace('//', '/', $targetPath) . $newFileName . "." . $fileParts['extension'];
                    move_uploaded_file($tempFile, $targetFile);

                    if ($folder == "photos") {
                        
                        $uploadtype = $this->request->data('type');
                        
                        $width = 1;
                        $height = 1;

                        if ($uploadtype == "episode" || $uploadtype == "program") {
                            $width = 128;
                            $height = 100;
                        }
                        
                        if ($uploadtype == "ustad") {
                            $width = 200;
                            $height = 150;
                        }
                        
                        if ($uploadtype == "slider") {
                            $width = 750;
                            $height = 400;
                        }

                        $resizeObj = new resize($targetFile);
                        $resizeObj->resizeImage($width, $height, 'crop');
                        $resizeObj->saveImage($targetFile, 100);
                    }

                    $nameexplode = explode('/', $targetFile);
                    $name = array_pop($nameexplode);
                    $type = $_FILES['Filedata']['type'];
                    $size = $_FILES['Filedata']['size'];
                    $status = 1;
                    $message = 'File successfully uploaded';
                } else {
                    $status = 0;
                    $message = 'Invalid file type.';
                }
            } else {
                $status = 0;
                switch ($_FILES['Filedata']['error']) {
                    case 1:
                        $message = 'File exceeded max filesize';
                        break;
                    case 2:
                        $message = 'File exceeded max filesize';
                        break;
                    case 3:
                        $message = 'File only partially uploaded';
                        break;
                    case 4:
                        $message = 'No file was uploaded';
                        break;
                    case 7:
                        $message = 'There was a problem saving the file';
                        break;
                    default:
                        $message = 'There was a problem uploading the file';
                        break;
                }
            }
        } else {
            $status = 0;
            $message = 'No file data received.';
        }
        echo json_encode(
                array(
                    'status' => $status,
                    'name' => $name,
                    'type' => $type,
                    'size' => $size,
                    'message' => $message
                )
        );
    }

    function upload() {
        $uploadDir = '/img/uploads/photos/';

        if (!empty($_FILES)) {
            debug($_FILES);

            //   $tempFile   = $_FILES['Filedata']['tmp_name'][0];
            $uploadDir = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;
            $targetFile = $uploadDir . $_FILES['Filedata']['name'][0];

            // Validate the file type
            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions
            $fileParts = pathinfo($_FILES['Filedata']['name'][0]);

            // Validate the filetype
            if (in_array($fileParts['extension'], $fileTypes)) {

                // Save the file

                $tempFile = time() . "_" . basename($_FILES['Filedata']['name'][0]);
                $_POST['image'] = $tempFile;

                move_uploaded_file($tempFile, $targetFile);



                $this->Photo->create();
                if ($this->Photo->save($_POST)) {
                    $this->Session->setFlash($targetFile, 'default', array('class' => 'alert_success'));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                // The file type wasn't allowed
                //echo 'Invalid file type.';
                $this->Session->setFlash(__('The photo could not be saved. Please, try again.', true));
            }
        }
    }

}

?>
