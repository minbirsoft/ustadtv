<?php

class FeedController extends AppController {
    
    public $components = array('RequestHandler');
    //public $helpers = array('Text');
    
    public function allepisodes() {
        $this->loadModel('Episode');
        
        $posts = $this->Episode->find('all', array(
            'order' => 'Episode.id DESC'
        ));
        $this->set('posts', $posts);
    }
    
}

?>
