<?php

class QuestionsController extends AppController {
    
    /* Administrator actions*/ 
    
    public function listquestions() {
        
        onlyAdmin($this);
        
        
        $this->paginate = array(
            'limit' => 30,
            'order' => array(
                'Question.id' => 'DESC'
            )
        );
        
        $data = $this->paginate('Question');
           
        $this->set('questions', $data);
    }
    
    
    public function deletequestion($id) {
        
        onlyAdmin($this);
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Question->delete($id)) {
            
            $this->Session->setFlash($id . ' nömrəli sual silindi.');
            $this->redirect(array('action' => 'listquestions'));
            
        }
    }
    
}

?>
