<?php

class ChannelsController extends AppController {
    
    public function index() {

        $channels = $this -> Channel -> find('all');
        $this->set('channels', $channels);
        
    }
    
    public function allchannels() {
        
        $this ->loadModel('Channel');
        
        $channels = $this -> Channel -> getAllMainWithChildren();
        $this->set('channels', $channels);
        
    }
    
    public function achannel($channelid) {
        
        $this ->loadModel('Channel');
        
        $channels = $this -> Channel -> getParentWithChildren($channelid);
        $this->set('channel', $channels);
        
    }
    
    /* Administrator actions */
    
    public function listchannels() {
        
        onlyAdmin($this);
        
        $listby = 'none';
        $withid = 0;
        
        if (isset($this->params['url']['listby'])) {
            $listby = $this->params['url']['listby'];
            $withid = $this->params['url']['withid'];
        }
        
        if ('channel' == $listby) {
            $channels = $this -> Channel -> find('all', array(
                'conditions' => array('Channel.parent_id' => $withid),
                'recursive' => 0
            ));
        } else {
            $channels = $this -> Channel -> find('all', array(
                'recursive' => 0
            ));
        }
           
        $this->set('channels', $channels);
    }
    
    public function addchannel() {
        
        onlyAdmin($this);
        
        if($this -> request -> is("post")) {
            
            $this->Channel->create();
            if ($this->Channel->save($this->request->data)) {
                $this->Session->setFlash('Kanal əlavə edildi.');
                $this->redirect(array('action' => 'listchannels'));
            } else {
                $this->Session->setFlash('Kanal əlavə edilə bilmədi');
            }
            
        } else {
            $channels = $this -> Channel -> getAllMainList();
            $this->set('channels', $channels);
        }
    }
    
    
    public function editchannel($id) {
        
        onlyAdmin($this);
        
        $this->Channel->id = $id;
        if ($this->request->is('get')) {
            $channels = $this -> Channel -> getAllMainList();
            $this->set(compact('channels'));
            $this->request->data = $this->Channel->read();
            
        } else {
            if ($this->Channel->save($this->request->data)) {
                $this->Session->setFlash('Dəyişiklik saxlanıldı');
                $this->redirect(array('action' => 'listchannels'));
            } else {
                $this->Session->setFlash('Dəyişiklik saxlanıla bilmədi.');
            }
        }
    }
    
    public function deletechannel($id) {
        
        onlyAdmin($this);
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Channel->delete($id)) {
            $this->Session->setFlash($id . ' nömrəli kanal silindi.');
            $this->redirect(array('action' => 'listchannels'));
        }
    }
}

?>
