<?php

class MainController extends AppController {

    var $components = array('Session', 'Email', 'RequestHandler');

    public function homepage() {

        $this->loadModel('Episode');
        $this->loadModel('Program');
        $this->loadModel('Ustad');
        $this->loadModel('Channel');
        
        $mostviewed3 = $this->Episode->find('all', array(
        		'fields' => array('Episode.id', 'Episode.slug', 'Episode.title', 'Episode.viewcount'),
        		'order' => array('Episode.viewcount DESC'),
        		'limit' => 3,
        		'recursive' => 0
        ));
        
        $mostliked3 = $this->Episode->find('all', array(
        		'fields' => array('Episode.id', 'Episode.slug', 'Episode.title', 'Episode.viewcount'),
        		'order' => array('Episode.likecount DESC'),
        		'limit' => 3,
        		'recursive' => 0
        ));

        $this->set('lastepisodes', $this->Episode->getLastN(6));
        $this->set('lastprograms', $this->Program->getLastN(6));
        $this->set('last3slides', $this->Episode->get3Slides());
        $this->set('mostviewed3', $mostviewed3);
        $this->set('mostliked3', $mostliked3);
        $this->set('someustads', $this->Ustad->getNRandom(6));
        $this->set('channels', $this->Channel->getAllMain());
        $this->set('title_for_layout', 'Azərbaycanın ilk video ensiklopediyası');
    }

    public function channel($id = null, $slug = null) {

        $this->loadModel('Episode');
        $this->loadModel('Program');
        $this->loadModel('Ustad');
        $this->loadModel('Channel');

        $channel = $this->Channel->find('first', array(
            'conditions' => array('Channel.id' => $id),
            'recursive' => 0
                ));

        if (!$channel) {
            throw new NotFoundException('Belə kanal mövcud deyil');
        }


        if (isset($channel['Channel']['parent_id'])) { //if channel is child
            $lastprograms = $this->Program->getLastNInChannelOffsetN($id, 6, 0);
            $childrenchannels = $this->Channel->getChildrenOf($channel['Channel']['parent_id']);
        } else { // if channel is parent
            $lastprograms = $this->Program->getLastNOfAMainChannel(6, $id);
            $childrenchannels = $this->Channel->getChildrenOf($id);
        }

        $this->set('channel', $channel);
        $this->set('parentchannels', $this->Channel->getAllMain());
        $this->set('childrenchannels', $childrenchannels);

        $this->set('lastprograms', $lastprograms);

        $this->set('title_for_layout', $channel['Channel']['name']);
    }

    public function channels() {

        $this->loadModel('Channel');

        $channels = $this->Channel->getAllMainWithChildren();

        $this->set('channels', $channels);
        $this->set('title_for_layout', 'Kanalların siyahısı');
    }

    public function programs() {

        $this->loadModel('Program');
        $this->loadModel('Channel');

        $listby = 'none';
        $withid = 0;

        if (isset($this->params['url']['listby'])) {
            $listby = $this->params['url']['listby'];
            $withid = $this->params['url']['withid'];
        }

        if ('channel' == $listby) {
            $conditions = array('Program.channel_id' => $withid);
        } else if ('ustad' == $listby) {
            $conditions = array('Program.ustad_id' => $withid);
        } else {
            $conditions = array();
        }

        $this->paginate = array(
            'limit' => 16,
            'order' => array(
                'Program.id' => 'DESC'
            ),
            'conditions' => $conditions
        );

        $programs = $this->paginate('Program');

        $this->set('programs', $programs);
        $this->set('channels', $this->Channel->getAllMain());
        $this->set('title_for_layout', 'Proqramların siyahısı');
    }

    public function program($id = null, $slug = null) {

        $this->loadModel('Channel');
        $this->loadModel('Program');


        $program = $this->Program->find('first', array(
            'conditions' => array('Program.id' => $id)
                ));
        
        if (!$program) {
            throw new NotFoundException('Belə proqram mövcud deyil');
        }

        $this->set('program', $program);
        $this->set('channels', $this->Channel->getAllMain());

        $this->set('title_for_layout', $program['Program']['title']);
        $this->set('meta_description', $program['Program']['description']);
        $this->set('meta_keywords', $program['Program']['tags']);
    }

    public function ustads($pro = null) {
        $this->loadModel('Ustad');
        $this->loadModel('Channel');
        
        $conditions = array(
        		'limit' => 20,
        		'order' => array(
        				'Ustad.id' => 'DESC'
        		),
        		'recursive' => 0
        );
        
        if (isset($pro)) {
        	$conditions['conditions'] = array('Ustad.profession' => $pro);
        	$this->set('pro', $pro);
        	$this->set('title_for_layout', 'Ustadlar → İxtisas: '.$pro);
        } else {
        	$this->set('title_for_layout', 'Ustadlar');
        }

        $this->paginate = $conditions;

        $ustads = $this->paginate('Ustad');

        $this->set('ustads', $ustads);
        $this->set('channels', $this->Channel->getAllMain());
    }

    public function ustad($id = null) {

        $this->loadModel('Program');
        $this->loadModel('Ustad');
        $this->loadModel('Channel');

        $ustad = $this->Ustad->find('first', array(
            'conditions' => array('Ustad.id' => $id),
            'recursive' => 0
                ));
        
        if (!$ustad) {
            throw new NotFoundException('Ustad tapılmadı');
        }

        $programs = $this->Program->find('all', array(
            'conditions' => array('Program.ustad_id' => $id),
            'recursive' => 0
                ));

        $stats = $this->Ustad->getStats();

        $this->set('programs', $programs);
        $this->set('ustad', $ustad);
        $this->set('stats', $stats);
        $this->set('channels', $this->Channel->getAllMain());

        $this->set('title_for_layout', $ustad['Ustad']['fullname']);
        $this->set('meta_description', $ustad['Ustad']['info']);
    }

    public function episode($id = null, $slug = null) {
        $this->loadModel('Episode');
        $this->loadModel('Adversitement');
        $this->loadModel('User');

        $episode = $this->Episode->find('first', array(
            'conditions' => array('Episode.id' => $id),
            'recursive' => 2
                ));
        
        if (!$episode) {
            throw new NotFoundException('Belə cavab mövcud deyil');
        }
        
        // incrase view count
        $this->Episode->updateAll(array('Episode.viewcount'=>'Episode.viewcount+1'), array('Episode.id'=>$id));

        //$ad = $this->Adversitement->getAdForChannel($episode['Program']['Channel']['id']);
        //$episode['Adversitement'] = $ad['Adversitement'];

        $this->set('episode', $episode);
        
        $hasliked = false;
        
        if(isLoggedIn($this)){
        	$hasliked = $this->User->hasLiked($this->Session->read('Auth.User.id'), $id);
        }

        $this->set('title_for_layout', $episode['Episode']['title']);
        $this->set('meta_description', $episode['Episode']['description']);
        $this->set('meta_keywords', $episode['Episode']['tags']);
        $this->set('hasliked', $hasliked);
    }
    
    

    public function askquestion() {

        //onlyUser($this, "Sual vermək üçün hesabınıza daxil olmalısınız. Hesabınız yoxdursa qeydiyyatdan keçin");

        if ($this->request->is("post")) {
            $this->loadModel('Question');
            // add
            $this->Question->create();
            $this->request->data['Question']['user_id'] = /*$this->Session->read('Auth.User.id')*/ 1 ; // insert without user_id

            if ($this->Question->save($this->request->data)) {
                $this->Session->setFlash('Sual əlavə edildi.', 'flash_message', array('type' => 'success'));
                $this->redirect(array('action' => 'homepage'));
            } else {
                $this->Session->setFlash('Sual əlavə edilə bilmədi. Yenidən yoxlayın', 'flash_message', array('type' => 'error'));
            }
        }

        $this->loadModel('Channel');
        $this->set('channels', $this->Channel->getAllMain());
        $this->set('title_for_layout', 'Ustadlardan soruşun');
    }

    public function login() {
        $this->loadModel('Channel');
        $this->set('channels', $this->Channel->getAllMain());
        $this->set('title_for_layout', 'Hesabınıza daxil olun');
    }

    public function signup() {
        $this->loadModel('Channel');
        $this->loadModel('User');

        $this->set('title_for_layout', 'Qeydiyyat');
        $this->set('channels', $this->Channel->getAllMain());

        if ($this->request->is("post")) {

            $this->User->create($this->data);

            if ($this->User->validates()) {

                if ($this->User->save()) {
                    $this->Session->setFlash('Qeydiyyat uğurlu oldu. Giriş edə bilərsiniz', 'flash_message', array('type' => 'success'));
                    $this->redirect(array('controller' => 'main', 'action' => 'login'));
                } else {
                    $this->Session->setFlash('Məlumatlar saxlanılarkən xəta baş verdi. Yenidən yoxlayın', 'flash_message', array('type' => 'error'));
                }
            }
        }
    }

    public function search() {

        $this->loadModel('Episode');
        $this->loadModel('Channel');

        // check if request came from search button, make query visible in url
        if (isset($this->request->data['query'])) {
            return $this->redirect(array('controller' => 'main', 'action' => 'search', '?' => array('query' => $this->request->data['query'])));
        }

        // now url is visible, check for empty
        if (!isset($this->params['url']['query']) || empty($this->params['url']['query'])) {
            $results = null;
            $query = null;
        } else {
            $query = $this->params['url']['query'];


            // query is not empty. search!
            $this->paginate = array(
                'limit' => 20,
                'conditions' => array(
                    'or' => array(
                        'Episode.title LIKE' => "%" . $query . "%",
                        'Episode.description LIKE' => "%" . $query . "%",
                        'Episode.tags LIKE' => "%" . $query . "%"
                    )
                )
            );

            $results = $this->paginate('Episode');
        }

        $this->set('results', $results);
        $this->set('channels', $this->Channel->getAllMain());
        $this->set('title_for_layout', 'Axtarış: "' . $query . '"');
    }

    public function contact() {
        $this->loadModel('Channel');

        if ($this->request->is("post")) {

            $this->Email->to = 'contact@ustadtv.az';
            $this->Email->subject = 'UstadTv - Əlaqə';
            $this->Email->from = $this->request->data['ContactData']['email'];
            $this->Email->template = 'contact';
            $this->Email->smtpOptions = array(
                'port' => '25',
                'timeout' => '30',
                'host' => 'mail.ustadtv.az',
                'username' => 'contact@ustadtv.az',
                'password' => 'contact',
            );
            $this->Email->delivery = 'smtp';

            $this->set('fullname', $this->request->data['ContactData']['fullname']);
            $this->set('message', $this->request->data['ContactData']['text']);

            if ($this->Email->send()) {
                $this->Session->setFlash('İsmarışınız göndərildi. Ən qısa zamanda cavablamağa çalışacağıq', 'flash_message', array('type' => 'success'));
                return $this->redirect(array('controller' => 'main', 'action' => 'homepage'));
            } else {
                //echo $this->Email->smtpError;
                $this->Session->setFlash('İsmarışınız göndərilə bilmədi. Xahiş edirik yenidən yoxlayın', 'flash_message', array('type' => 'error'));
            }
        }

        if (isLoggedIn($this)) {
            $user = $this->Session->read('Auth.User');
            $fullname = $user['fullname'];
            $email = $user['email'];
        } else {
            $fullname = '';
            $email = '';
        }

        $this->set('fullname', $fullname);
        $this->set('email', $email);
        $this->set('channels', $this->Channel->getAllMain());
        $this->set('title_for_layout', 'Əlaqə');
    }

}
