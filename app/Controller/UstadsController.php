<?php

class UstadsController extends AppController {
    
    
    /* Administrator actions */
    
    public function listustads() {
        
        onlyAdmin($this);
        
        $ustads = $this -> Ustad -> find('all', array('recursive' => 0));
        
        $this->set('ustads', $ustads);
    }
    
    public function addustad() {
        
        onlyAdmin($this);
        
        if($this -> request -> is("post")) {

            $this->Ustad->create();
            if ($this->Ustad->save($this->request->data)) {
                $this->Session->setFlash('Ustad əlavə edildi.');
                $this->redirect(array('action' => 'listustads'));
            } else {
                $this->Session->setFlash('Ustad əlavə edilə bilmədi');
            }
        }
    }
    
    
    public function editustad($id) {
        
        onlyAdmin($this);
        
        $this->Ustad->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->Ustad->read();
            
        } else {
            if ($this->Ustad->save($this->request->data)) {
                $this->Session->setFlash('Dəyişiklik saxlanıldı');
                $this->redirect(array('action' => 'listustads'));
            } else {
                $this->Session->setFlash('Dəyişiklik saxlanıla bilmədi.');
            }
        }
    }
    
    public function deleteustad($id) {
        
        onlyAdmin($this);
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Ustad->delete($id)) {
            $this->Session->setFlash($id . ' nömrəli ustad silindi.');
            $this->redirect(array('action' => 'listustads'));
        }
    }
    
}

?>
