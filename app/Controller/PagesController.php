<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Pages';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public function index() {

        $this->autoRender = false;

        $this->loadModel('Channel');
        $this->loadModel('Program');
        $this->loadModel('Episode');
        $this->loadModel('User');
        $this->loadModel('Ustad');
        $this->loadModel('Adversitement');

        $channels = $this->Channel->find('all');

        $ustadid = 3;


        $queryEpisodeCount = "SELECT COUNT(Episode.id) as episodes, SUM(Episode.viewcount) as viewcount, COUNT(DISTINCT Episode.program_id) as programscount  " .
                "FROM " .
                "   `ustadtv`.`episodes` as Episode " .
                "WHERE " .
                "   Episode.program_id IN (SELECT Program2.id FROM `ustadtv`.`programs` as Program2 WHERE Program2.ustad_id = 2) ";

        $testdata = $this->Episode->query($queryEpisodeCount);

        pr($testdata);


        $this->set('channels', $channels);
        $this->set('testdata', $testdata);
    }

}
