<?php

class UsersController extends AppController {
	
	public $components = array('Session');
    
    public function login() {
    	
    	$this->autoRender = false;
        
        if($this -> request -> is("post")) {
            
            $user = $this -> User -> getWithUsernameAndPassword($this -> data['LoginData']['username'], $this -> data['LoginData']['password']);
            
            $this->Session->write('Auth', $user);
            
            if (!$user) {
                $this->Session->setFlash('Giriş detalları düzgün deyil. Yenidən yoxlayın', 'flash_message', array('type' => 'error'));
                return $this->redirect(array('controller' => 'main','action' => 'login'));
            } else {
                $this->Session->write('Auth', $user);
                $this->Session->setFlash('Hesabınıza daxil oldunuz', 'flash_message', array('type' => 'success'));
                return $this->redirect('/');
            }
            
        }
        
    }
    
    public function logout() {
        $this->Session->delete('Auth');
        return $this->redirect('/');
    }
    
    public function signup() {
        if($this -> request -> is("post")) {
            
            $this -> User -> create($this->data);
            
            if ($this->User->validates()) {
                
                if($this->User->save()){
                    $this->Session->setFlash('Qeydiyyat uğurlu oldu. Giriş edə bilərsiniz', 'flash_message', array('type' => 'success'));
                    $this->redirect(array('controller' => 'main','action' => 'login'));
                } else {
                    $this->Session->setFlash('Məlumatlar saxlanılarkən xəta baş verdi', 'flash_message', array('type' => 'error'));
                }
                
            }
            
        }
    }
    
    public function panel() {
        
        onlyUser($this);
        
        $this->autoRender = false;
        
        pr($this -> Session -> read('Auth'));
    }
    
    /* Login with Facebook */
    
    public function facebook($authorize = null) {
    	
    	$this->autoRender = false;
    	
		App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));
		
		$facebook = new Facebook(array(
				'appId'  => Configure::read("FB_APP_ID"),
				'secret' => Configure::read("FB_APP_SECRET"),
		));
    	
		$fbuser = $facebook->getUser();
    
    	// not logged into Facebook and not a callback either,
    	// sending user over to Facebook to log in
    	if (!$fbuser && !$authorize) {
    		$params = array(
    				'scope'  => 'email',
    				'next'       => Router::url(array('action' => 'facebook', 'authorize'), true),
    				'cancel_url' => Router::url(array('action' => 'login'), true)
    		);
    		$this->redirect($facebook->getLoginUrl($params));
    	}

    
    	// user is coming back from Facebook login,
    	// assume we have a valid Facebook session
    	$userInfo = $facebook->api('/me');
    
    	if (!$userInfo) {
    		// nope, login failed or something went wrong, aborting
    		$this->Session->setFlash('Facebook login failed');
    		$this->redirect(array('action' => 'login'));
    	}

    	// if registered user have already connected his fb account
    	$user = $this->User->getWithFacebookUid($userInfo['id']);
    	if($user) {
    		//login
    		$this->Session->write('Auth', $user);
    		$this->Session->setFlash('Facebook vasitəsilə hesabınıza daxil oldunuz', 'flash_message', array('type' => 'success'));
    		return $this->redirect('/');
    	}
    	
    	// if registered user connects fb for the first time
    	$user = $this->User->getWithEmail($userInfo['email']);
    	if($user) {
    		
    		//add facebook uid to user's account
    		$user['User']['facebook_uid'] = $userInfo['id'];
    		$this->User->save($user);
    		// now login
    		$this->Session->write('Auth', $user);
    		$this->Session->setFlash('Facebook vasitəsilə hesabınıza daxil oldunuz', 'flash_message', array('type' => 'success'));
    		return $this->redirect('/');
    	}
    	
    	// unregistered user logs in for the for the first time
    	$user = array(
    		'User' => array(
    				'fullname'		=> $userInfo['name'],
    				'username'		=> trim(parse_url($userInfo['link'], PHP_URL_PATH), '/')."-".$userInfo['id'],
    				'email'			=> $userInfo['email'],
    				'facebook_uid'	=> $userInfo['id']
    		)
    	);
    	// save user
    	$this->User->create();
    	$this->User->save($user);
    	// now login
    	$this->Session->write('Auth', $user);
    	$this->Session->setFlash('Facebook vasitəsilə hesabınıza daxil oldunuz', 'flash_message', array('type' => 'success'));
    	return $this->redirect('/');
    	
    }
    
    
    /* Administrator actions*/ 
    
    public function listusers() {
        
        onlyAdmin($this);
        
        $this->paginate = array(
            'limit' => 30,
            'order' => array(
                'User.id' => 'DESC'
            )
            //'conditions' => array()
        );
        
        $data = $this->paginate('User');
        
        //$users = $this -> User -> find('all');
           
        $this->set('users', $data);
    }
    
    public function adduser() {
        
        onlyAdmin($this);
          
        if($this -> request -> is("post")) {
            // insert post
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('İstifadəçi əlavə edildi.');
                $this->redirect(array('action' => 'listusers'));
            } else {
                $this->Session->setFlash('İstifadəçi əlavə edilə bilmədi');
            }
        }
    }
    
    public function edituser($id) {
        
        onlyAdmin($this);
         
        $this->User->id = $id;
        if ($this->request->is('get')) {

            $this->request->data = $this->User->read();
            
        } else {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Dəyişiklik saxlanıldı');
                $this->redirect(array('action' => 'listusers'));
            } else {
                $this->Session->setFlash('Dəyişiklik saxlanıla bilmədi.');
            }
        }
    }
    
    public function deleteuser($id) {
        
        onlyAdmin($this);
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->User->delete($id)) {
            $this->Session->setFlash($id . ' nömrəli istifadəçi silindi.');
            $this->redirect(array('action' => 'listusers'));
        }
    }
    
    /* Ajax Like */
    
    public function likeepisode() {
    	
    	$this->autoRender = false;
    	$this->loadModel('Episode');
    	$this->loadModel('User');
    	
    	
    	$episode_id = $this->params['url']['episode'];
    	
    	//if (isLoggedIn($this) ){
    		
    		$user_id = $this->Session->read('Auth.User.id');
    		$this->User->like(/*$user_id*/ 0, $episode_id);
    		
    		$data = Array(
    				"answer" => "liked"
    		);
    		
    	//} else {
    	//	
    	//	$data = Array(
    	//			"answer" => "notloggedin"
    	//	);
    	//	
    	//}
    	
    	echo json_encode($data);
    	
    }
    
}
