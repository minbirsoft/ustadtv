<?php


class EpisodesController extends AppController {
    
    /* Administrator actions*/ 
    
    public function listepisodes($programid = null) {
        
        onlyAdmin($this);
        
        $listby = 'none';
        $withid = 0;
        
        if (isset($this->params['url']['listby'])) {
            $listby = $this->params['url']['listby'];
            $withid = $this->params['url']['withid'];
        }
        
        if ('program' == $listby) {
            $conditions = array('Episode.program_id' => $withid);
        } else {
            $conditions = array();
        }
        
        $this->paginate = array(
            'limit' => 30,
            'order' => array(
                'Episode.id' => 'DESC'
            ),
            'conditions' => $conditions
        );
        
        $data = $this->paginate('Episode');
        
        
           
        $this->set('episodes', $data);
    }
    
    public function addepisode() {
        
        onlyAdmin($this);
        
        $this ->loadModel('Program');
        $this ->loadModel('Adversitement');
          
        if($this -> request -> is("post")) {
            $this->Episode->create();
            if ($this->Episode->save($this->request->data)) {
                $this->Session->setFlash('Epizod əlavə edildi.');
                $this->redirect(array('action' => 'listepisodes'));
            } else {
                $this->Session->setFlash('Epizod əlavə edilə bilmədi');
            }
        } else {
            $programs = $this -> Program -> find('list');
            $this->set('programs', $programs);
            $adversitements = $this -> Adversitement -> find('list');
            $this->set('adversitements', $adversitements);
        }
    }
    
    public function editepisode($id) {
        
        onlyAdmin($this);
        
        $this ->loadModel('Program');
        $this ->loadModel('Adversitement');
         
        $this->Episode->id = $id;
        if ($this->request->is('get')) {
            $programs = $this -> Program -> find('list');
            $this->set(compact('programs'));
            $adversitements = $this -> Adversitement -> find('list');
            $this->set('adversitements', $adversitements);
            $this->request->data = $this->Episode->read();
            
        } else {
            if ($this->Episode->save($this->request->data)) {
                $this->Session->setFlash('Dəyişiklik saxlanıldı');
                $this->redirect(array('action' => 'listepisodes'));
            } else {
                $this->Session->setFlash('Dəyişiklik saxlanıla bilmədi.');
            }
        }
    }
    
    public function deleteepisode($id) {
        
        onlyAdmin($this);
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Episode->delete($id)) {
            $this->Session->setFlash($id . ' nömrəli epizod silindi.');
            $this->redirect(array('action' => 'listepisodes'));
        }
    }
    
}

?>
