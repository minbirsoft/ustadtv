$(document).ready(function() {
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
        
        var height = $(window).height();
        height = height - 74;
        $('#content').css('min-height', height);
        
	$('.vertmenuitem').hover(function () {
		$(this).animate({'border-left-width' : '3px','padding-left' : '5px' },200);
		$(this).css({'background-color' : '#ffffff', 'width' : '265px'});
	}, function () {
		$(this).animate({'border-left-width' : '0px', 'padding-left' : '0px'},75);
		$(this).css({'background-color' : 'transparent'});
	});
        
        $('#fblogin').hover(function(){
            $(this).css({'background-color' : '#26aee5'});
        },function(){
            $(this).css({'background-color' : 'transparent'});
        });
        
        $('.menuitemnormal').hover(function(){
            $(this).animate({'border-top-width' : '2px'}, 100);
            $(this).css({'background-color' : '#ffffff'});
        }, function(){
            $(this).animate({'border-top-width' : '0px'}, 100);
            $(this).css({'background-color' : 'transparent'});
        });
        
        $('#searchdiv').hover(function(){
            $('#search').css({'border-bottom-left-radius' : '0px'});
            $('#searchbox').css('display', 'inherit')
            $('#searchbox').animate({'width' : '175px'}, 400);
        },function(){
            var searchword = $('#searchbox').attr('value');
            if(searchword==''){
            $('#searchbox').animate({'width' : '0px'}, 100, function(){
               $('#search').css({'border-bottom-left-radius' : '3px'});
               $('#searchbox').css('display', 'none');
            });
            }
        });
        
        
        
        
        
}); // end ready

function likebuttonclickevent($episode_id) {
	$.ajax({
		  url: '../users/likeepisode',
		  data: {episode: $episode_id },
		  dataType: "json",
		  success: function(data) {
		    
		    if(data.answer == "notloggedin") {
		    	
		    	var n = noty({
		            text: 'Cavabı bəyənmək üçün hesabınıza daxil olmalısınız',
		            type: 'error',
		            dismissQueue: true,
		            layout: 'center',
		            theme: 'defaultTheme',
		            buttons: [
		            	{addClass: 'btn btn-primary', text: 'OK', onClick: function($noty) {
							$noty.close();
		                    }
		                }
		            ]
              });
		    	
		    } else if(data.answer == "liked") {
		    	
		    	var n = noty({
		            text: 'Səsiniz qeydə alındı. Təşəkkürlər',
		            type: 'success',
		            layout: 'center',
		            timeout: 2000,
		            theme: 'defaultTheme'
              });
		    	
		    	$("#episodelike").removeClass("hasnotliked").addClass("hasliked").html("Bunu bəyənirsiniz");
		    	
		    } else {
		    	
		    }
		    
		  }
		});
}