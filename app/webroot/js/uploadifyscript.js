function uploadifyIt(element, input, buttontext, folder, timestamp, token, extensions, type ){
    $('#'+element).uploadify({
                'formData'     : {
                    'timestamp' : timestamp,
                    'token'     : token,
                    'folder'    : folder,
                    'type'      : type
                },
    
                'swf'      : '/ustadtv/uploadify/uploadify.swf',
                'uploader'  : '/ustadtv/upload/ajaxUpload',
                'cancelImg' : '/ustadtv/uploadify/cancel.png',
                'auto'      : true,
                'multi'     : false,
                'sizLimit'  : 31457280,
                'onUploadSuccess': function(file, data, response){
                    var responseObj = $.parseJSON(data);
                    $('#'+input).val(responseObj.name);
                },
                'buttonText': buttontext,
                'fileExt'   : extensions
            });
}