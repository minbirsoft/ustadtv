<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 * 
 */
        Router::parseExtensions('rss', 'xml');
        
	Router::connect('/', array('controller' => 'main', 'action' => 'homepage'));
        
        Router::connect('/kanallar', array('controller' => 'main', 'action' => 'channels'));
        
        Router::connect('/proqramlar', array('controller' => 'main', 'action' => 'programs'));
        
        Router::connect('/ustadlar', array('controller' => 'main', 'action' => 'ustads'));
        
        Router::connect('/ustadlar/:pro', array('controller' => 'main', 'action' => 'ustads'), array('pass'=>array('pro')));
        
        Router::connect('/bizdensorush', array('controller' => 'main', 'action' => 'askquestion'));
        
        Router::connect('/girish', array('controller' => 'main', 'action' => 'login'));
        
        Router::connect('/qeydiyyat', array('controller' => 'main', 'action' => 'signup'));
        
        Router::connect('/elaqe', array('controller' => 'main', 'action' => 'contact'));
        
        Router::connect('/axtar', array('controller' => 'main', 'action' => 'search'));
        
        Router::connect('/kanal/:id-:slug', 
                array('controller' => 'main', 'action' => 'channel'),
                array('pass' => array('id', 'slug')));
        
        Router::connect('/proqram/:id-:slug', 
                array('controller' => 'main', 'action' => 'program'),
                array('pass' => array('id', 'slug')));
        
        Router::connect('/cavab/:id-:slug', 
                array('controller' => 'main', 'action' => 'episode'),
                array('pass' => array('id', 'slug')));
        
        Router::connect('/ustad/:id', 
                array('controller' => 'main', 'action' => 'ustad'),
                array('pass' => array('id')));
        
        Router::connect('/sitemap', array('controller' => 'sitemap', 'action'=> 'sitemap', 'ext' => 'xml'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on 
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
